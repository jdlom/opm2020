---
title: "Import donnees Navettes domicile-travail"
author: "Th Z"
date: "1/07/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Il s'agit d'importer les données relatives aux déplacements domicile → lieu de travail, issues du recensement de la population.

### I.1.  Resultat attendu  

Un tableau unique comportant les informations suivantes :
- origine (domicile) / destination (lieu de travail) à l'échelle communale, agrégeables à l'EPCI  
- distance entre le domicile et le lieu de travail : calculée comme la distance entre les centres des deux communes.
- mode de transport utilisé  
- coordonnées géographiques permettant de localiser la commune du domicile et la commune du lieu de travail  
- distance entre la commune de depart et la commune d'arrivee (en km), calculee entre le centre des communes.
- classe de distance :
   - 0 a 1km
   - 1km a 5km,
   - 5km a 10km,
   - 10km a 50km,
   - +50km
- deux indicateurs caractérisant le lieu de travail par rapport au lieu du domicile.
- nombre de ménage correspondant à chaun des croisements de ces informations.

La données géographique n'est pas conservée, est conservée seulement les coordonnées X et Y du centre des communes. La non conservation des données géographique permet de ne pas surcharger les données. La conservation des coordonnées X et Y permet de ne pas devoir recalculer ces coordonnées ni faire une jointure lorsque l'on souhaitera les afficher sous forme de carte.

### I.2 donnees disponibles

#### fichier source :  INSEE

**source** : Source des donnees : https://www.insee.fr/fr/statistiques/4507890


**Telechargement** :  
Le millesime 2017, paru le 29/06 est disponible : 
"https://www.insee.fr/fr/statistiques/fichier/4507890/RP2017_mobpro_csv.zip"


**Fichiers** :  le fichier **RP2017_mobpro_csv** contient 32 variables et 7 861 201  observations.  
L'archive ZIP  contient egalement un fichier contenant les metadonnees et les modalites des variables.
 les donnees des fichiers details sont restituees dans la geographie de production, soit au 1er janvier 2019. 

**Detail des donnees (info du site Insee) :**  
    - Donnees bilocalisees domicile-travail a la commune, decrivant les caracteristiques de l'individu, de son menage et de sa residence principale,  
    - variables de tri : commune de residence / commune de travail (y compris commune de travail situee dans les pays frontaliers),  
    - champ : individus actifs ayant un emploi et ages de 15 ans ou plus. 


Le fichier national mobilites professionnelles des individus : deplacements commune de residence / commune de travail est disponible au format dBase (.dbf) ou Csv (.csv).  

A partir de ce fichier, seules les donnees de la region Grand Est sont conservees et rattachees a leur EPCI d'appartenance (les flux domicile/lieu de travail en provenance ou à destination du Grand Est)




### I.3. Donnees necessaires
Les donnees doivent permettre de detailler, pour chaque EPCI, les navettes domicile-travail, reliant commune de domicile a commune (ou EPCI) de lieu de travail.  

Dans le cas des deplacements frontaliers :
  Choix de récuperer la nomenclature NUTS, relative aux communes européenne. C'est une nommenclature interne à l'Insee, pas en acces libre au grand public.  
  Source : https://ec.europa.eu/eurostat/fr/web/nuts/local-administrative-units

#### Donnees geographiques
(communes a tous les indicateurs) :
- communes de domicile
- communes de lieu de travail
- rattachement des communes a leur EPCI
- coordonnees geographiques des communes en vue de realisation de cartes : récupération des coordonnées X et Y du centre des communes (centroïd du polygone)
- localisation de chaque EPCI (departement et region du siege)

#### Donnees demographiques :
- mode de transport
- indicateur de distance
- nombre de flux concernes

Les donnees devront pouvoir etre agregees a l'EPCI, au departement et a la region.
Pour cela, la composition communales des EPCI doit etre importee, presente dans le COG.  






### I.4. description du script
Le script importe d'abord les donnees, verifie le COG (COG 2019)
L'import se fait depuis le fichier ZIP directement, en extrayant les fichiers temporairement dans le repertoire temporaire de R. Apres lecture, le fichier extrait temporairement est supprime.
Puis, il restreint les donnees aux commune des EPCIs de la region (domicile et/ou lieu de travail).  
Il les associe alors a leur EPCI et determine les coordonnees geographiques, y compris pour les travailleurs frontaliers.
Les coordonnees geographiques permettent de calculer une distance (a vol d'oiseau).

#### Filtres et avertissements sur les données

- on retire toutes les navettes dont le lieu de travail est inconnu (Sans objet ou "autre  commune etrangere")
- quelques communes-iles ont le code ZZZZZZZZZ pour representer leur epci.
- les communes ayant pour region le code ZZ "Collectivites d'outre-mer" n'ont pas d'EPCI associe
- la commune 21213 correspondant à Crimolois n'a pas d'EPCI associe. C'est une commune qui a fusionne pour devnir la commune nouvelle Neuilly-Crimolois (21452) créée le 28/02/2019.
- Quelques communes d'Allemagne et de Suisse n'ont pas d'equivalent dans la table de passage NUTS. Monaco n'a pas non plus d'equivalent. Ces communes ne sont donc pas rattachees a un territoire equivalent EPCI.

### II. Debut de l'import


```{r packages, message=FALSE}

library(dplyr)
library(readr)
library(readxl)
library(data.table)
library(stringr)
# remotes::install_github("antuki/COGugaison") # pour premiere utilisation
library(COGugaison)
library(here)
library(sf)

# Sauver les donnees dans "inst/donnees_rds"
if (!dir.exists(here("inst/"))) {dir.create(here("inst"))}
if (!dir.exists(here("inst/donnees_rds"))) {dir.create(here("inst/donnees_rds"), recursive = TRUE)}
```



#### II.1. Import Donnees

si le fichier est absent, le telecharge dans le dossier `data-raw/donnees_mobpro`.

#### Fichier deja telecharge ?
```{r}
nfzip <- "data-raw/donnees_mobpro/RP2017_MOBPRO_csv.zip"
urlzip <- "https://www.insee.fr/fr/statistiques/fichier/4507890/RP2017_mobpro_csv.zip"

if(!dir.exists(here("data-raw/donnees_mobpro"))) {
  dir.create(here("data-raw/donnees_mobpro"))
}


if(!file.exists(nfzip)) {
    download.file(urlzip,here(nfzip))
    message("Fichier telecharge a l'instant")
} else {
        message("deja telecharge")
}
file.size(nfzip)
```


Lecture du contenu de l'archive ZIP

```{r}
(liste_fich_zip <- 
    unzip(zipfile = here(nfzip), exdir = tempdir(),
          list = T) %>%
    arrange(desc(Length))
 )
```


#### Import des donnees CSV

### fonction d'import du CSV depuis le ZIP
- [X] extraction de puis le ZIP dans le repertoire tempo
- [X] lecture des donnees

```{r}
import_fread_csv <- function(nfzip, nfcsv) {
  unzip(zipfile = nfzip, files = nfcsv,
        exdir = tempdir())
  
  # determination du nombre de champs, qui sera utilise dans la fonction data.table::fread()
  nb_champs <- 
    read_lines(file = file.path(tempdir(), nfcsv), n_max = 1) %>% 
    str_count( ";") + 1
  
  fread(file = file.path(tempdir(),nfcsv),
                       sep = ";",
                       stringsAsFactors = F,
                       colClasses = rep("character", 
                                        times = nb_champs)
  )
}
```

### Appel de la fonction : import donnees 
```{r}
system.time(
d_mobpro_fr <- import_fread_csv(nfzip = nfzip,
                                nfcsv = liste_fich_zip$Name[1]
                                )
) # 30 s

if(exists("d_mobpro_fr")) {
    unlink(file.path(tempdir(), liste_fich_zip$Name[1])) # efface fichier tempo
  }
```


### Appel de la fonction : import metadonnees  

```{r}
var_mobpro_fr <- import_fread_csv(nfzip = nfzip,
                                nfcsv = liste_fich_zip$Name[2]
                                ) # %>% 
  #filter(!(COD_VAR %in% c("ARM","COMMUNE", "DCLT")))

if(exists("var_mobpro_fr")) {
    unlink(file.path(tempdir(), liste_fich_zip$Name[2])) # efface fichier tempo
  }
```

#### Arrondissement PLM
Le code_com.lt de Paris, Lyon et Marseille sont en fait les arrondissements.
On les convertit, pour le calcul des distances ulterieur. Sinon, le calcul plantera (les codes arrond. sont absents des cartes).
De plus, on supprime la colonne `ARM` (arrondissement du lieu de travail), inutile.


```{r}
d_mobpro_fr <- d_mobpro_fr %>% 
  mutate(DCLT =
               case_when(
                   DCLT %in% "75101":"75120" ~ "75056",  # Paris
                   DCLT %in% "69381":"69389" ~ "69023",  # Lyon
                   DCLT %in% "13201":"13216" ~ "13055",  # Marseille
                   TRUE ~ DCLT
                   )
         )  %>%    select(-ARM)
```


### renommage de champs
Les variables que l'on souhaite conserver sont renommées pour correspondre aux règles de nommages prévues :
- noms explicites
- que des minuscules
- homogénéité des champs similaires : code_com pour le code insee des communes, code_reg pour celui des régions,...
On modifie en conséquence le dictionnaire des variables.

```{r}
names(d_mobpro_fr) %>% head(30)
```

Attention au nommage des colonnes DCLT et DCFLT : le F indique "Frontalier", mais dans certains fichiers , c'est le contraire ! (domicile-etudes par ex.)


```{r}
d_mobpro_fr <- d_mobpro_fr %>% 
  rename(ipondi = IPONDI,
         code_com.dom = COMMUNE,
         code_com.lt = DCLT,
         code_com.ltf = DCFLT,  # frontalier
         code_reg.dom = REGION,
         code_reg.lt = REGLT,
         mode_transport = TRANS,
         indic_lieu_travail = ILT,
         indic_urbain_lieu_travail = ILTUU
         )


var_mobpro_fr <- var_mobpro_fr %>% 
  mutate(COD_VAR = case_when(
    COD_VAR == "ILTUU" ~ "indic_urbain_lieu_travail",
    COD_VAR == "ILT" ~ "indic_lieu_travail",
    COD_VAR == "TRANS" ~ "mode_transport",
    COD_VAR == "IPONDI" ~ "ipondi",
    COD_VAR == "COMMUNE" ~ "code_com.dom",
    COD_VAR == "DCLT" ~ "code_com.lt",
    COD_VAR == "DCFLT" ~ "code_com.ltf",
    COD_VAR == "REGION" ~ "code_reg.dom",
    COD_VAR == "REGLT" ~ "code_reg.lt",
    TRUE ~ COD_VAR)
    )

```



### Verification du COG
```{r}
d_mobpro_fr %>% 
  filter(code_com.dom != "99999") %>% sample_n(size = 10000) %>% 
  pull(code_com.dom) %>% 
  COG_akinator(donnees_insee = T)

```
Les donnees sont normalement au COG 2019.




### Filtre sur donnees non renseignees
On retire navettes sans commune de lieu de travail identifiées : dont la modalité indique un travail à l'étranger (code "99999") et que la commune du lieu de travail à l'étranger indique  Sans objet ou autre commune étrangère (code "ZZZZZ").

##### Verification des donnees non conservees
```{r, eval=FALSE}

# nombre de Navettes supprimees, par commune de domicile
nav_sans_lt <- d_mobpro_fr %>% 
  filter(code_com.ltf == "ZZZZZ") %>% 
  group_by(code_com.dom) %>% 
  mutate(nb_nav = sum(as.numeric(ipondi))) %>% 
  ungroup() %>%
  distinct(code_com.dom, nb_nav) %>% 
  arrange(desc(nb_nav))

# Nombre de navettes sur toute la base
nb_nav <- d_mobpro_fr %>% 
  mutate(ipondi = as.numeric(ipondi)) %>% 
  pull(ipondi) %>% sum()

# Nombre de navettes sans lieu de travail identifié
nb_sans_lt <- nav_sans_lt %>% 
  pull(nb_nav) %>% sum()

### Proportion de navettes supprimees
nb_sans_lt / nb_nav * 100    # inf. a 0,1%
```

Donnees filtrees :
```{r}
d_mobpro_fr <- d_mobpro_fr %>% 
  filter( code_com.ltf != "ZZZZZ")
```


### Transformation de la variable mode_transport en factor
```{r}
d_mobpro_fr <- d_mobpro_fr %>% 
  left_join(var_mobpro_fr %>% 
              filter(COD_VAR == "mode_transport") %>% 
              select(COD_MOD, LIB_MOD) %>% 
              mutate(factor_mode_transport = factor(COD_MOD, labels = LIB_MOD)),
            by = c("mode_transport" = "COD_MOD")) %>% 
    select(-mode_transport, -LIB_MOD) %>% 
    rename(mode_transport = factor_mode_transport)
```




### III. Donnees pour la region Grand Est

La selection des communes se fait grace aux donnees du COG, deja chargees.
Cela permet d'incorporer les communes hors region 44 mais appartenant a des EPCI de la region (3 communes de Haute-Saone).

#### Chargement donnees EPCI et communes 

```{r}
l_epci_fr <- read_rds(file = here("inst/donnees_rds/l_epci_fr.rds"))
l_epcicom_fr <- read_rds(file = here("inst/donnees_rds/l_epcicom_fr.rds"))

# liste des EPCI de la region 44
l_epci_44 <- l_epci_fr %>% filter(reg_siege == "44")

# liste des communes des EPCI de la region 44
l_epcicom_44 <- l_epcicom_fr %>%
  filter(code_epci %in% pull(l_epci_44, code_epci))

# liste des codes de communes de la region 44
l_com44 <- l_epcicom_44 %>% pull(code_com)
```

### III.1. Filtre des donnees pour la Region Grand Est

On inclut les donnees de communes de domicile **et** de lieu de travail (operateur logique OR).

```{r}
d_mobpro_44 <- d_mobpro_fr %>% 
  filter(code_com.dom %in% l_com44 |
           code_com.lt %in% l_com44)
```

### Effacement du fichier global
Permet un gain en memoire vive.

```{r}
rm("d_mobpro_fr")
```


### regroupement des variables lieu de travail et lieu de travail frontalier
Regroupement des codes commune dans une seule variable, et creation de `commune.ltf` : commune frontaliere ou non 

Pour les communes frontalieres, la region sera le pays correspondant, et sera les 2 premiers caractere du code de la commune frontaliere. Pour plus de coherence avec le referentiel NUTS, on notera DE pour l'Allemagne (plutot que AL) et CH pour la Suisse (plutot que SU)

```{r}
d_mobpro_44 <- d_mobpro_44 %>% 
  mutate(code_com.lt = ifelse(code_com.ltf == "99999",
                              code_com.lt,
                              code_com.ltf
                              ),
         code_reg.lt =  ifelse(code_com.ltf == "99999",
                               code_reg.lt, 
                               str_sub(code_com.ltf, 1, 2)
                               ),
         code_reg.lt =  ifelse(code_reg.lt == "AL",
                               "DE",
                               code_reg.lt
                               ),
         code_reg.lt =  ifelse(code_reg.lt == "SU",
                               "CH",
                               code_reg.lt
                               ),
         commune.ltf =  ifelse(code_com.ltf == "99999",
                               FALSE, TRUE)
         ) # mutate
```

La colonne `code_com_ltf` n'est plus necessaire. Elle ne sera pas incluse dans l'agregation ci-apres.

### IV.  agregation des donnees
Passage de 595 Klignes a 131 Klignes x 9 col

Dans le group_by, on peut choisir d'ajouter d'autres variables si on souhaite les conserver pour les indicateurs.
A ce stade, seules indic_lieu_travail (Indicateur du lieu de travail), indic_urbain_lieu_travail (Indicateur urbain du lieu de travail) et mode_transport (Mode de transport) (modalités décrites dans les metadonnees) sont conservees.

```{r}
d_mobpro_agreg <- d_mobpro_44 %>% 
  mutate(ipondi = as.numeric(ipondi)) %>% 
  group_by(code_reg.dom, code_reg.lt, code_com.dom,
           code_com.lt, commune.ltf, indic_lieu_travail,
           indic_urbain_lieu_travail, mode_transport) %>% 
  summarise(nb_navettes = round(sum(ipondi, na.rm = T))) %>% 
  ungroup()

```


#### IV.1. Association a EPCI France
Pour les communes francaises, association a leur EPCI  
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>%
  left_join(select(l_epcicom_fr, code_com, code_epci),
            by = c("code_com.dom" = "code_com")) %>%
  left_join(select(l_epcicom_fr, code_com, code_epci),
            by = c("code_com.lt" = "code_com"),
            suffix = c(".dom", ".lt"))
```

#### Vérification des potentielles anomalies ou données manquantes concernant l'EPCI associé à chaque commune.
```{r}
#- Les EPCI et le nombre de navettes associées.
epci_entrants_nav <- d_mobpro_agreg %>% 
  filter(commune.ltf == FALSE) %>%  
  group_by(code_epci.lt) %>%
  mutate(nb_navettes = sum(nb_navettes)) %>% 
  ungroup() %>% 
  distinct(code_epci.lt, nb_navettes) %>% 
  arrange(desc(nb_navettes))

epci_sortants_nav <- d_mobpro_agreg %>% 
  filter(commune.ltf == FALSE) %>%  
  group_by(code_epci.com) %>% 
  mutate(nb_navettes = sum(nb_navettes)) %>%
  ungroup() %>%
  distinct(code_epci.com, nb_navettes) %>%
  arrange(desc(nb_navettes))

# Les lignes correspondants aux communes sans EPCI ou associees a l'EPCI ZZZZZZZZZ
 epci_manquants <- d_mobpro_agreg %>% 
   filter(commune.ltf == FALSE) %>% 
   filter(code_epci.dom == "ZZZZZZZZZ" | is.na(code_epci.lt))
 
 # Toutes les communes de la region ZZ n'ont pas ete associees a un EPCI.
  epci_manquants <- d_mobpro_agreg %>% 
    filter(commune.ltf == FALSE & code_reg.lt == "ZZ") 
```


### IV.2 Pays frontaliers
La codification des communes frontalieres (colonne DCLTF renommee code_com.ltf) est etablie selon une nomenclature interne a l'INSEE. La table des correspondance (source INSEE) avec les codes NUTS3 permet de rattacher des coordonnees geographiques a un niveau supra-communal. Ce fichier a ete demande par Van  en juin 2020 a des collegues de l'INSEE.
Cette table de passage est au cOG 2013. !!


### Association du code NUTS au code com.lt
Le champ CODGEO2013 contient le code INSEE. 
Le champ NUTS contient le code que l'on retrouve dans les fichiers carto. 
CODE_EURO est le code des unites administratives locales (LAU), non cartographies. 
Source : https://ec.europa.eu/eurostat/fr/web/nuts/local-administrative-units
liste : https://eur-lex.europa.eu/legal-content/FR/ALL/?uri=CELEX:02003R1059-20180118&qid=1519136585935


#### Import de la table de correspondance

La procedure est la meme que pour les fichiers de donnees.
Extraction prealable dans le reptemp du fichier zippe

```{r}
nfxlszip <- "data-raw/donnees_nuts/TableCommunes_FR_BE_CH_DE_LU.zip"

(liste_fich_zip <-
    unzip(zipfile = nfxlszip, exdir = tempdir(),
          list = T) %>%
    arrange(desc(Length))
 )
nfxls <- liste_fich_zip$Name[1]

  unzip(zipfile = nfxlszip, files = nfxls,
        exdir = tempdir())
```


```{r}
# file.exists(nfxls)
 excel_sheets(file.path(tempdir(),nfxls))
```

```{r}
t_nuts <-read_excel(file = file.path(tempdir(),nfxls),
                    sheet = "TableCommunes_FR_BE_CH_DE_LU",
                    col_types = "text")
```

### Effacer le fichier tempo XLS
```{r}
unlink(file.path(tempdir(),nfxls))
```

### IV.2. Recuperation des infos des pays frontaliers


```{r}
 t_nuts %>% glimpse()
```

La fusion se fera donc avec NUTS3.NOM_ASCI est le libelle des communes sans accent ni caractere special. Il n'est pas utilise ni conserve ici.
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  left_join(select(t_nuts, NUTS3, CODGEO2013, NOM_ASCI),
            by = c("code_com.lt" = "CODGEO2013")) %>% 
  mutate(code_epci.lt = ifelse(is.na(code_epci.lt), NUTS3, code_epci.lt)) %>% 
  # rename(lib_com.lt = NOM_ASCI) %>% 
  select(-NUTS3, -NOM_ASCI)
```

Verification des communes etrangeres pour lesquelles on n'a pas recupere de code Nuts
```{r}
# Le nombre de navettes associe a chaque nuts
epciltf_nbnav <- d_mobpro_agreg %>% 
  filter(commune.ltf == TRUE) %>% 
  group_by(code_epci.lt) %>% 
  summarise(nb_nav = sum(nb_navettes)) %>% 
  ungroup()

# Les communes sans Nuts
epciltf_manquants <- d_mobpro_agreg %>%
  filter(commune.ltf == TRUE & is.na(code_epci.lt))

```
Quelques communes d'Allemagne et de Suisse n'ont pas d'equivalent dans la table de passage NUTS. Monaco n'a pas non plus d'equivalent.
Puisqu'il n'existe qu'un seul NUTS de niveau 3 au luxembourg, valant LU000, On peut redresser toutes les communes de lieu de travail au luxembourg par ce code NUTS.
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  mutate(code_epci.lt = ifelse(code_reg.lt == "LU",
                               "LU000",
                               code_epci.lt)
         ) 
```



## Sauvegarde locale, temporaire
Essais de sauvegarde, avec ou non compression des donnees : 
le temps de sauvegarde est proche.
```{r}
# system.time(
# write_rds(d_mobpro_44, file = "mobpro_provi_gz.rds", 
#           compress = "gz")
# ) # 12 s
# system.time(
# write_rds(d_mobpro_44, file = "mobpro_provi.rds")
# ) # 14 s
```

# Temps de lecture
Lecture des fichiers compresses ou non : le temps de lecture est proche.
```{r}
# system.time(
# d_mobpro_44bz <- read_rds( file = "mobpro_provi_gz.rds")
# ) # 7.7 s
# system.time(
# d_mobpro_44b <- read_rds( file = "mobpro_provi.rds")
# ) # 7.4 s
```

### V. Ajout de coordonnes geographiques
Les coordonnees permettront le calcul de la distance a vol d'oiseau

#### V.1. Coordonnees France
#### Carte des communes et des EPCI
La carte des communes de France n'a pas ete conservee pour l'utilisation au sein de l'appli. Elle est re-importee ici, afin de recuperer les coordonnees XY.
Il convient de s'assurer que le dossier cartes_admin_express exist eet contienne les cartes en format SHP.

```{r}
c_com_fr <- st_read(dsn = "data-raw/cartes_admin_express/COMMUNE.shp")
c_epci_fr<- st_read(dsn = "data-raw/cartes_admin_express/EPCI.shp")
```


### Fonction d'ajout des coordonnees
Cette fonction extrait les coordonnees X et Y des centroides des polygones des couches SHP.
Elle les associe a la carte entree en parametre.

```{r}
ajoute_coord_a_carte <- function(x) {
  if("X" %in% names(x) &
     "Y" %in% names(x)) {
    message("Coordonnees deja calculees")
  } else {
    x <-  x %>%
      bind_cols(
        as.data.frame(x %>% 
                        st_geometry() %>% 
                        st_centroid() %>% 
                        st_coordinates()
        )
      )
  }
  return(x)
}
```

### dataframe contenant juste le code INSEE (renommé dans le select) et les coordonnees 
```{r}
d_com_fr <- c_com_fr %>% ajoute_coord_a_carte() %>% 
  as.data.frame() %>% select(code_com = INSEE_COM, X, Y) %>% 
  mutate(code_com = as.character(code_com))

d_epci_fr <- c_epci_fr %>% ajoute_coord_a_carte()%>% 
  as.data.frame()  %>% select(code_epci = CODE_EPCI, X, Y) %>% 
  mutate(code_epci = as.character(code_epci))
```

# Ajout des coordonnees
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  left_join(d_com_fr, by = c("code_com.dom" = "code_com"))%>% 
  left_join(d_com_fr, by = c("code_com.lt" = "code_com"),
            suffix = c(".com.dom", ".com.lt"))  %>% 
  left_join(d_epci_fr, by = c("code_epci.dom" = "code_epci"))%>% 
  left_join(d_epci_fr, by = c("code_epci.lt" = "code_epci"),
            suffix = c(".epci.dom", ".epci.lt"))

```



#### V.2. Coordonnees des entites frontalieres

Import carte des regions NUTS

```{r}
c_nuts <- st_read(dsn = "data-raw/donnees_nuts/nuts2013-level3.geojson")
st_crs(c_nuts)$input
```

Les coordonnees sont en WGS 84. Les convertir en Lambert93
```{r}
c_nuts_lambert <- c_nuts %>% st_transform(crs = 2154)
st_crs(c_nuts_lambert)$input
```

```{r}
d_nuts <- c_nuts_lambert %>% ajoute_coord_a_carte() %>% 
  as.data.frame() %>%  select(-geometry) %>% 
  mutate(id = as.character(id),
         name = as.character(name))
```

### Ajout des coordonnees des lt frontaliers
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  left_join(d_nuts, by = c("code_epci.lt" = "id"))%>% 
  mutate(X.epci.lt = ifelse(is.na(X.epci.lt), X, X.epci.lt),
         Y.epci.lt = ifelse(is.na(Y.epci.lt), Y, Y.epci.lt))  %>% 
  select(-X, -Y) %>% 
  select(-CC) %>% 
  rename(lib_epci.front = name)
```


### VI. Calcul  de distance domicile-travail (communes et entites)
Les coordonnees etant en Lambert93, (unite metrique), le calcul de distance peut se faire de facon simple par Pythagore.  
On ne calcule la distance qu'entre des communes dont on connait les coordonnees (le centre de la commune) : ce qui veut dire qu'on ne calcule pas de distance lors de flux impliquant des communes etrangeres.
```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  mutate(distance_km = round(sqrt((X.com.dom - X.com.lt)**2 +
                              (Y.com.dom - Y.com.lt)**2) / 1000) ,
         cl_distance = case_when(  is.na(distance_km) | distance_km < 0 ~ "Inconnue",
                       distance_km < 1 ~ "Moins de 1 km",
                       distance_km < 5 ~ "1 - 5 km",
                       distance_km < 10 ~ "5 - 10 km",
                       distance_km < 50 ~ "10 - 50 km",
                       distance_km >= 50 ~ "Pus de 50 km",
                               TRUE ~ "Inconnue") %>% 
             factor(levels=c("Moins de 1 km", 
                             "1 - 5 km", 
                             "5 - 10 km",
                             "10 - 50 km", 
                             "Plus de 50 km",
                             "Inconnue"))
         ) %>% 
  select(starts_with("code_"), everything()) 
```


## Retrait des donnees non rattachees a un EPCI de depart et d'arrivee
```{r}
d_mobpro_agreg %>% 
  filter(is.na(code_epci.dom) | is.na(code_epci.lt)) %>% summarise(sum(nb_navettes))

d_mobpro_agreg <- d_mobpro_agreg %>% 
 # On retire les flux sans EPCI de depart ou d'arrivee
    filter(!is.na(code_epci.dom) &
             !is.na(code_epci.lt))
```


## Renommage
On renomme les variables suffixees par dom en les suffixant par depart.  
On renomme les variables suffixees par lt ou etu en les suffixant par arrivee.

```{r}
d_mobpro_agreg <- d_mobpro_agreg %>% 
  rename(code_com_depart = code_com.dom,
         code_com_arrivee = code_com.lt,
         code_epci_depart = code_epci.dom,
         code_epci_arrivee = code_epci.lt,
         X.com_depart = X.com.dom,
         X.com_arrivee = X.com.lt,
         X.epci_depart = X.epci.dom,
         X.epci_arrivee = X.epci.lt,
         Y.com_depart = Y.com.dom,
         Y.com_arrivee = Y.com.lt,
         Y.epci_depart = Y.epci.dom,
         Y.epci_arrivee = Y.epci.lt)

```


### VII. Sauvegarde RDS

La compression "gz" produit des fichiers plus compacts, a peine plus long a produire que les fichiers non compresses.
```{r}
write_rds(d_mobpro_agreg,
          file = here("inst/donnees_rds/d_mobpro_r44.rds"), 
          compress = "gz")
write_rds(var_mobpro_fr,
          file = here("inst/donnees_rds/var_mobpro.rds"), 
          compress = "gz")

```



