---
title: "import cartes"
author: "ThZ"
date: "06/05/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Préambule

Du fait de leur volume, les données ne sont pas présentes dans le repo Git. 
Elles pourront être téléchargées depuis le site FTP de l'IGN (Cf URL ci-après), puis les fichiers nécessaires copiés dans le dossier local `data_raw/cartes_admin_express`.



### Lien vers téléchargement

*Le nom du fichier pourra évoluer selon les millésimes.*

```{r}
# version buggée avec mauvais "oe"
# url <- "ftp://Admin_Express_ext:Dahnoh0eigheeFok@ftp3.ign.fr/ADMIN-EXPRESS-COG_2-1__SHP__FRA_L93_2020-03-25.7z.001"
# pas mieux.
# url <- "ftp://Admin_Express_ext:Dahnoh0eigheeFok@ftp3.ign.fr/ADMIN-EXPRESS-COG_2-1__SHP__FRA_L93_2020-07-29.7z"
# Préférable de récupérer les SHP depuis le serveur SIG de la DREAL

```

Depuis l'archive, les fichiers suivants ont été copiés dans le répertoire local à l'appli en vue de leur utilisation :
- REGION.shp (+ dépendances)
- DEPARTEMENT.shp (+ dépendances)
- COMMUNE.shp (+ dépendances)
- EPCI.shp (+ dépendances)

autres fichiers de l'archive, non importés , mais disponibles :
- ENTITE_RATTACHEE.shp
- CANTON.shp
- ARRONDISSEMENT_DEPARTEMENTAL.shp
- CHEF_LIEU.shp



## packages
```{r}
library(sf)
library(dplyr)
library(readr)
library(here)
library(stringr)
```

## Vérification de l'existence des données

```{r}
if(!dir.exists(here("data-raw/cartes_admin_express"))) {
  message("Le repertoire cartes_admin_express n'existe pas")
} else {
  message("repertoire cartes_admin_express OK")
}
```

### Fonction remplacement de caractères
Cette fonction peut être employée pour des cartes SHP téléchargées depuis l'IGN, où les "oe" sont mal encodés.
Si tel est le cas (le vérifier au moment de l'import), employer cette fonction.
L'alternative est d'utiliser les fichiers SHP du serveur SIG de la DREAL.
Liens: 
https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode/U0100
https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode/U0080#Commandes_C1_et_latin_%C3%A9tendu_%E2%80%93_1_(Unicode_1.1)
```{r}

remplace_oe <- function(x) {
  # mauvais_oe <-  "½"
  mauvais_oe <- "\u00BD"
  # bon_oe <- "œ"
  bon_oe <- "\u0153"
  
  str_replace_all(string = x,
                  pattern = mauvais_oe,
                  replacement = bon_oe)
}

## Exemple d'utilisation
ville_test <- "Vand½uvre-lès-Nancy"

remplace_oe(ville_test)

```

#### 1. Import REGION

```{r}
carte_reg_fr <- read_sf(here("data-raw/cartes_admin_express/REGION.shp"))
```

- [X] Vérification de référentiel : L93

```{r}
st_crs(carte_reg_fr)$input
```

- [X] Renommage des champs et suppression du nom en majuscules (inutilisé)

```{r}
carte_reg_fr %>% names()
```

```{r}
carte_reg_fr <- carte_reg_fr %>% 
  select(-NOM_REG_M) %>% 
  rename(id = ID, lib_reg = NOM_REG, code_reg = INSEE_REG, code_pref_reg = CHF_REG)
```

- [X] Vérification

```{r}
carte_reg_fr %>% names()
```

- [X] Aperçu de la couche

```{r}
plot(st_geometry(carte_reg_fr))
```



#### 2. Import DEPARTEMENT.shp

```{r}
carte_dep_fr <- read_sf(here("data-raw/cartes_admin_express/DEPARTEMENT.shp"))
```

- [X] Vérification de référentiel : L93

```{r}
st_crs(carte_dep_fr)$input
```

- [X] Renommage des champs et suppression du nom en majuscules (inutilisé)

```{r}
carte_dep_fr %>% names()
```

```{r}
carte_dep_fr <- carte_dep_fr %>% 
  select(-NOM_DEP_M) %>% 
  rename(id = ID, lib_dep = NOM_DEP, code_dep = INSEE_DEP,
         code_reg = INSEE_REG, code_pref_dep = CHF_DEP)
```

- [X] Vérification

```{r}
carte_dep_fr %>% names()
```


- [X] Aperçu de la couche

```{r}
plot(st_geometry(carte_dep_fr))
```


#### 3. Import COMMUNE.shp
```{r}
carte_communes_fr <- read_sf(here("data-raw/cartes_admin_express/COMMUNE.shp"))
```

- [X] Vérification de référentiel : L93

```{r}
st_crs(carte_communes_fr)$input
```

- [X] Renommage des champs et suppression du nom en majuscules (inutilisé)

```{r}
carte_communes_fr %>% names()
```

```{r}
carte_communes_fr <- carte_communes_fr %>% 
  select( -NOM_COM_M, -INSEE_CAN,
         -INSEE_ARR,  -POPULATION) %>% 
  rename(id = ID,
         lib_com = NOM_COM,
         code_com = INSEE_COM,
         statut_com = STATUT, 
         # type_com = TYPE,
         # code_canton = INSEE_CAN, code_arrond = INSEE_ARR,
         code_dep = INSEE_DEP, 
         code_reg = INSEE_REG, code_epci = CODE_EPCI
         )
```

- [X] Vérification

```{r}
carte_communes_fr %>% names()
```

#### statut_com comporte les infos suivantes :  Il peut être souhaitable des le conserver.
```{r}
carte_communes_fr$statut_com %>% table
```


- [X] Aperçu de la couche

```{r}
# plot(st_geometry(carte_communes_fr)) # long à afficher
plot(st_geometry(
  sample_n(carte_communes_fr, 1000))
  )
```

#### 4. Import EPCI

```{r}
carte_epci_fr <- read_sf(here("data-raw/cartes_admin_express/EPCI.shp"))
```

- [X] Vérification de référentiel : L93

```{r}
st_crs(carte_epci_fr)$input
```


- [X] Renommage des champs et suppression du nom en majuscules (inutilisé)

```{r}
carte_epci_fr %>% names()
```

```{r}
carte_epci_fr <- carte_epci_fr %>% 
  # select(-NOM_EPCI) %>%
  rename(id = ID ,
         code_epci = CODE_EPCI,
         lib_epci = NOM_EPCI,
         type_epci = TYPE_EPCI)
```

- [X] Vérification

```{r}
carte_epci_fr %>% names()
```

- [X] Aperçu de la couche

```{r}
plot(st_geometry(carte_epci_fr))
```


### Cartes région 44

Code de la région à retenir :
```{r}
code_reg_courant <- "44"
```

#### 1. carte REGION 44
```{r}
carte_reg_r44 <- carte_reg_fr %>% 
  filter(code_reg == code_reg_courant)
plot(st_geometry(carte_reg_r44))
```

#### 2. carte DEPARTEMENT

```{r}
carte_dep_r44 <- carte_dep_fr %>% 
  filter(code_reg == code_reg_courant)
plot(st_geometry(carte_dep_r44))
```

#### 3. carte EPCI

La couche EPCI ne possède pas le champ code_reg.
Le filtre peut se faire d'après la liste des EPCI, `l_epci_fr`, chargée au préalable

`l_code_epci_44` : liste des codes des EPCI de la région 44

```{r}
l_epci_fr <- 
  read_rds(here("inst/donnees_rds/l_epci_fr.rds"))

l_code_epci_44 <- l_epci_fr %>% 
  filter(reg_siege == code_reg_courant) %>% 
  pull(code_epci)
```


```{r}
carte_epci_r44 <- carte_epci_fr %>% 
  filter(code_epci %in% l_code_epci_44)
plot(st_geometry(carte_epci_r44))
```

#### 3. carte COMMUNE

Il y a des communes d'EPCI de la région situées hors de la région (3 communes de Haute-Saône pour la région 44).
Le filtre doit se faire de préférence avec la table de rattachement des communes aux EPCI, `l_epcicom_fr`.
`l_code_com_epci_44` : liste des communes des EPCI de la région 44

```{r}
l_epcicom_fr <- 
  read_rds(here("inst/donnees_rds/l_epcicom_fr.rds"))

# l_epcicom_fr %>% names()
l_code_com_epci_44 <- l_epcicom_fr %>% 
  filter(code_epci %in% l_code_epci_44) %>% 
  pull(code_com)
```

```{r}
carte_communes_r44 <- carte_communes_fr %>% 
  filter(code_com %in% l_code_com_epci_44)

# plot(st_geometry(carte_communes_r44,500))
plot(st_geometry(sample_n(carte_communes_r44,600)))
```

### Sauvegarde RDS

La sauvegarde ne concerne pas la carte des communes de France, trop grosse et pas utilisée pour l'outil de portrait.

```{r}
# write_rds(carte_reg_fr,
#           path = "inst/donnees_rds/carte_reg_fr.rds",
          # compress = "gz")
write_rds(carte_reg_r44,
          path = here("inst/donnees_rds/carte_reg_r44.rds"),
          compress = "gz")
# write_rds(carte_dep_fr,
#           path = "inst/donnees_rds/carte_dep_fr.rds",
          # compress = "gz")
write_rds(carte_dep_r44,
          path = here("inst/donnees_rds/carte_dep_r44.rds"),
          compress = "gz")
# write_rds(carte_epci_fr, 
#           path = "inst/donnees_rds/carte_epci_fr.rds",
          # compress = "gz")
write_rds(carte_epci_r44,
          path = here("inst/donnees_rds/carte_epci_r44.rds"),
          compress = "gz")
# write_rds(carte_communes_fr,
          # path = "inst/donnees_rds/carte_communes_fr.rds",
          # compress = "gz")
write_rds(carte_communes_r44,
          path = here("inst/donnees_rds/carte_communes_r44.rds"),
          compress = "gz")
```

