---
title: "Import donnees equipement automobile des menages"
author: "Thierry ZORN"
date: "20/08/2020"
output: html_document

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Il s'agit d'importer les donnees concernant l'equipement automobile des menages, a savoir le nombre de voitures par menage.
Puis d'en calculer une proportion : % des menages possedant 

### Resultat attendu  

Un tableau unique comportant les informations suivantes :

- code_com le code de la commune 
- code_epci le code de l'EPCI de la commune
- nombre de ménage dans la commune
- nombre de ménages avec au moins un véhicule
- nombre de ménage avec 1 seul véhicule
- nombre de ménage avec 2 véhicules ou plus.


Doit permettre de realiser  trois cartes, par aplat de couleurs, représentant les 3 pourcentages decrits ci-dessus

Pour tous les EPCIs de la region Grand Est, tous les departements de la region Grand Est et pour la Region Grand Est.



### Donnees disponibles

Parmi les donnees du recensement de la population figurent des donnees sur les logements et les menages, et en particulier les donnees d'equipement automobile.

Les donnees, la documentation associee et un dictionnaire des variables sont accessible sur le site de l'Insee via : 
https://www.insee.fr/fr/statistiques/4171415


**Donnees importees**  
Le millesime 2016, paru le 25/06 est disponible.  
Les données 2016 sont proposées dans la géographie en vigueur au 1er janvier 2019.
Lien de telechargement : 
https://www.insee.fr/fr/statistiques/fichier/4171415/base_cc_log-2016-xls.zip



### Description du script
Le script importe d'abord le fichier zip disponible en telechargement si necessaire sur le site de l'Insee.  

Ensuite, le script importe les donnees communales, selectionne uniquement les variables necessaires et ne conserve que les communes des EPCIs du Grand Est.

L'import se fait depuis le fichier ZIP directement, en extrayant les fichiers temporairement dans le repertoire temporaire de R.    
On ne retient que les variables qui nous interessent :
- code de la commune, 
- P16_RP_VOIT1P : Nombre de ménages disposant au moins d'une voiture,
- P16_RP_VOIT1 : Nombre de ménages disposant d'une voiture
- P16_RP_VOIT2P : Nombre de ménages disposant de deux voitures ou plus
- P16_MEN : Nombre de ménages total

Les donnees sont enfin restreintes aux territoires du Grand Est. On aura besoin du fichier des EPCI issu du COG pour associer les communes a un EPCI

Pour finir, on renomme les champs pour qu'ils respectent les regles de nommages : pas de majuscule ni de caractere special, un nom explicite, les codes commencent par code_.



### Initialisation

Chargement des packages, verification de l'existence du dossier dans lequel nous sauvegarderons les donnees pretes a l'emploi, et creation du dossier s'il n'existe pas deja.

```{r packages, message=FALSE}

library(dplyr)
library(readr)
library(here)
library(stringr)
library(readxl)
library(COGugaison)

# Sauver les donnees dans "inst/donnees_rds"
if (!dir.exists(here("inst/"))) {dir.create(here("inst"))}
if (!dir.exists(here("inst/donnees_rds"))) {dir.create(here("inst/donnees_rds"), recursive = TRUE)}
```



### Import Donnees

si le fichier est absent, le telecharge a partir de l'url de telechargement, dans le dossier `data-raw/donnees_niveau_vie`.  
Fichier deja telecharge ?
```{r}
nfzip <- "data-raw/donnees_logement_menage/base_cc_log-2016-xls.zip"
urlzip <- "https://www.insee.fr/fr/statistiques/fichier/4171415/base_cc_log-2016-xls.zip"

if(!dir.exists(here("data-raw/donnees_logement_menage"))) {
  dir.create(here("data-raw/donnees_logement_menage"))
}


if(!file.exists(nfzip)) {
    download.file(urlzip,here(nfzip))
    message("Fichier telecharge a l'instant")
} else {
        message("deja telecharge")
}
file.size(nfzip)
```

Quels fichiers sont presents dans le zip ?
```{r}
    liste_fich_dans_zip <- unzip(zipfile = here(nfzip),
                                 exdir = tempdir(),
                                 list = T)
```


Extraction du XLS depuis le ZIP dans le rep temporaire dse R, lecture du contenu, et effacement du XLS 

```{r}
nfxls <- liste_fich_dans_zip$Name[1]

  unzip(zipfile = nfzip, 
        files = nfxls,
        exdir = tempdir())

    file.exists( file.path(tempdir(),nfxls))

  # Nom des onglets
  excel_sheets(path = file.path(tempdir(), nfxls))
```


```{r}
d_menages <- read_excel(path = file.path(tempdir(), nfxls),
                        sheet = "COM_2016",
                        skip = 5)

d_menages %>% glimpse()  
```




### Mise en forme des donnees

#### Selection des variables d'interet

On ne retient que les variables d'interet : le code du territoire (CODGEO) et les donnees relatives aux menages.
Il sera possible de revenir sur ce script, pour ajouter d'autres variables a incorporer, pour d'autres indicateurs de l'application.


```{r}
d_menages <- d_menages %>% 
  select(CODGEO, P16_MEN, contains("VOIT"))

d_menages %>% head()
```

### Renommage des champs

```{r}

d_menages <- d_menages %>%
  rename(code_com = CODGEO,
         nombre_de_menages = P16_MEN,
         menages_1v_plus = P16_RP_VOIT1P,
         menages_1seul_v = P16_RP_VOIT1,
         menages_2v_plus = P16_RP_VOIT2P
         )

```

#### Verification du COG

```{r}
COG_akinator(d_menages$code_com,
             donnees_insee = T)
```

```{r}
apparier_COG(vecteur_codgeo = d_menages$code_com, donnees_insee = T,
             COG=2020)
```

  
### Restriction des donnees au Grand Est

Chargement des donnees du COG donnant la composition des EPCI par commune.  
Recuperation des communes des EPCIs du Grand Est.
```{r}
l_epcicom_fr <- read_rds(path = here("inst/donnees_rds/l_epcicom_fr.rds"))

l_com_44 <- l_epcicom_fr %>% 
  filter(reg_siege == "44") %>% 
  pull(code_com)
```

 **Filtre des donnees pour la Region Grand Est** 

On ne retient que les donnees relatives au Grand Est.
```{r}
d_menages_r44 <- d_menages %>% 
  filter(code_com %in% l_com_44)

```



### Association des communes aux EPCI

```{r}
d_menages_r44 <- d_menages_r44 %>% 
  left_join(select(l_epcicom_fr, code_com, code_epci),
            by = "code_com") %>% 
  select(starts_with("code"), everything())

```


### Sauvegarde RDS

La compression "gz" produit des fichiers plus compacts, a peine plus long a produire que les fichiers non compresses.
```{r}
write_rds(d_menages_r44,
          path = here("inst/donnees_rds/d_menages_r44.rds"), 
          compress = "gz")

```

### Fin du script d'import.

## Suite : Exemples d'affichage de cartes

### Parametres generaux

Calcul des quantiles, pour essais de découpage des classes, en vue de leur représentation cartographique

```{r}
d_menages <- d_menages %>% 
  mutate(part_men_1seul_v = menages_1seul_v / nombre_de_menages * 100,
         part_men_1v_plus = menages_1v_plus / nombre_de_menages * 100,
         part_men_2v_plus = menages_2v_plus / nombre_de_menages * 100
         )
```


```{r}
d_menages %>% pull(part_men_1seul_v) %>% quantile(na.rm = T)

```


```{r}
d_menages %>% pull(part_men_1v_plus) %>% quantile(na.rm = T)

```


```{r}
d_menages %>% pull(part_men_2v_plus) %>% quantile(na.rm = T)

```


#### Région Grand Est

```{r}
d_menages_r44 <- d_menages_r44 %>% 
  mutate(part_men_1seul_v = menages_1seul_v / nombre_de_menages * 100,
         part_men_1v_plus = menages_1v_plus / nombre_de_menages * 100,
         part_men_2v_plus = menages_2v_plus / nombre_de_menages * 100,
         cl_part_men_1seul_v = cut(part_men_1seul_v, 
                                   breaks = seq(0,100,10)),
         cl_part_men_1v_plus = cut(part_men_1v_plus, 
                                   breaks = seq(0,100,10)),
         cl_part_men_2v_plus = cut(part_men_2v_plus, 
                                   breaks = seq(0,100,10))
         )
```

Calcul des quantiles, pour essais de découpage des classes, en vue de leur représentation cartographique

```{r}
(cl_1seul_v <- d_menages_r44 %>% 
  pull(part_men_1seul_v) %>% quantile(na.rm = T))
```


```{r}
(cl_1v_plus <- d_menages_r44 %>%
   pull(part_men_1v_plus) %>% quantile(na.rm = T))
```


```{r}
(cl_2v_plus <- d_menages_r44 %>%
  pull(part_men_2v_plus) %>% quantile(na.rm = T))
```


### Exemple de cartes

```{r}
library(sf)

carte_com <- 
  read_rds(path = here("inst/donnees_rds/carte_communes_r44.rds"))

code_epci_test <- "200030526"  # Marckolsheim
```


```{r}
# Ajout des donnees a la carte
carte_menages <- carte_com %>% select(-code_epci) %>% 
  merge(d_menages_r44,
        by.x = "code_com",
        by.y = "code_com"
        )
```

```{r}
#Créer une rampe de couleurs automatiquement
rampe_vehic <- colorRampPalette(colors=c("yellow","dark green"))

# carte de l'EPCI choisi
carte_menages_epci <- carte_menages %>% 
  filter(code_epci == code_epci_test) %>% 
  left_join(select(l_epcicom_fr, code_com, lib_com),
            by = "code_com")
```


#### Affichage de la carte simple
```{r}
carte_menages_epci %>% st_geometry() %>% plot()
```


#### Affichage des cartes de part des ménages  (%)

Persiste un pb de décalage des cartes superposées.

```{r}
carte_menages_epci %>% 
  select(part_men_1seul_v) %>% 
  plot(pal = rampe_vehic,
       main = "Part (%) des ménages possédant un seul véhicule",
       breaks = seq(0,100,10)
       )
carte_menages_epci %>% st_geometry() %>% plot(add = T)

#Ajout un point sur centre des villes
points(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     pch = "*")

#Ajout du nom des villes
text(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     carte_menages_epci$NOM_COM,
     pos=3,
     offset = 1,
     cex = 0.8)
```

```{r}
carte_menages_epci %>% 
  select(part_men_1v_plus) %>% 
  plot(pal = rampe_vehic,
       main = "Part (%) des ménages possédant au moins un véhicule",
       breaks = seq(0,100,10)
       )

#Ajout un point sur centre des villes
points(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     pch = "*")
text(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     carte_menages_epci$NOM_COM,
     pos=3,
     offset = 1,
     cex = 0.8)
```


```{r}
carte_menages_epci %>% 
  select(part_men_2v_plus) %>% 
  plot(pal = rampe_vehic,
       main = "Part (%) des ménages possédant deux véhicules ou plus",
       breaks = seq(0,100,10)
       )
#Ajout un point sur centre des villes
points(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     pch = "*")

text(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     carte_menages_epci$NOM_COM,
     pos=3,
     offset = 1,
     cex = 0.8)
```


```{r}
carte_menages_epci %>% 
  select(cl_part_men_1seul_v) %>% 
  plot(pal = rampe_vehic,
       main = "Part (%) des ménages possédant deux véhicules ou plus (classes)",
       breaks = seq(0,100,10)
       )
#Ajout un point sur centre des villes
points(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     pch = "*")

text(x=st_coordinates(st_centroid(carte_menages_epci))[,1],
     y=st_coordinates(st_centroid(carte_menages_epci))[,2],
     carte_menages_epci$NOM_COM,
     pos=3,
     offset = 1,
     cex = 0.8)
```

## Cartes avec ggplot2

```{r}
library(ggplot2)

ggplot(carte_menages_epci) +
    # scale_fill_gradient(low = "yellow", high = "dark green")+
   scale_fill_brewer(name = "Part des\n ménages\n (%)",
                     type = "seq", palette = "BuGn")+
  # scale_fill_viridis_d() +
  theme_bw() +
  geom_sf(aes(fill = cl_part_men_2v_plus)) +
  coord_sf(crs = 4326) +
  
  theme(axis.ticks = element_blank(),
        axis.text = element_blank(),
        axis.title = element_blank())+
  geom_sf_text(aes(label = lib_com),
               vjust=-0.5,
               check_overlap=TRUE,
               fontface="italic",colour="white") +
  
  geom_sf_label( ### essai avec sf_label. Choisir text OU label
    mapping = aes(label = lib_com),
    parse = FALSE,
    # nudge_x = 0,
    # nudge_y = 0,
    label.padding = unit(0.25, "lines"),
    label.r = unit(0.15, "lines"),
    label.size = 0.25,
    na.rm = FALSE
  ) +
  ggtitle("Part (%) des ménages possédant deux véhicules") +
  theme(title = element_text(size = 12),
        plot.margin = unit(c(0,0.1,0,0.25), "inches"))
```

