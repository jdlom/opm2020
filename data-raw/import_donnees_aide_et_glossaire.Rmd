---
title: "Import données aide et glossaire"
author: "ThZ"
date: "16/09/2020"
output: html_document
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### I.1. Données nécessaires

Les donnees du glossaire sont resaisies manuellement dans le fichier
data-raw/donnees_aide/texte_glossaire.txt.
Apres chaque modification, relancer ce script.


```{r packages, message=FALSE}
library(readr)
library(here)
```

# Import des donnees

```{r}
texte_glossaire <- read_file(
  file = here("data-raw/donnees_aide/texte_glossaire.txt"))
```

### Sauvegarde RDS

```{r}
write_rds(x = texte_glossaire,
          path = here("inst/donnees_rds/texte_glossaire.rds"),
          compress = "gz")
```


