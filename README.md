<!-- README.md is generated from README.Rmd. Please edit that file -->

# opm2020 : Outil Portrait Mobilité 2020

dev: [![pipeline
status](https://gitlab.com/drealge/opm2020/badges/dev/pipeline.svg)](https://gitlab.com/drealge/opm2020/-/commits/dev)
[![coverage
report](https://gitlab.com/drealge/opm2020/badges/dev/coverage.svg)](https://gitlab.com/drealge/opm2020/-/commits/dev)

## Application shiny

L’application est accessible via
<https://ssm-ecologie.shinyapps.io/opm2020/>

Un article sur l’Internet de la DREAL Grand Est présente l’application :
<http://www.grand-est.developpement-durable.gouv.fr/outil-portrait-mobilite-a19915.html>

## Développement de l’application

L’application a été développée par la DREAL Grand Est, au sein du
Service Connaissance et Développement Durable.

### Agilité

Formés à l’agilité par la société Naomis (<https://www.naomis.fr/>), ce
service a mis en place un fonctionnement selon la méthode SCRUM, pour le
développement de cette application web.

  - Product Manager : François MATHONNET  
  - Product Owner : Sophie TRICARD
  - Scrum Master : Guillaume VANEY
  - Développeurs : Thierry ZORN et Guillaume VANEY

### R - ThinkR

ThinkR (<https://thinkr.fr/>) nous a formé au développement à plusieurs
avec Git et GitLab, d’une application RShiny dans un package R avec
golem (<https://github.com/ThinkR-open/golem>). ThinkR nous a ensuite
accompagné tout au long du développement de cette application. Cet
accompagnement, une demi-journée toute les trois semaines, nous a permis
d’identifier et de résoudre les divers points de blocages que nous
pouvions rencontrer lors du développement. Un accompagnement idéal pour
valoriser et consolider la formation dispensée.

Nous les remercions chaleureusement.

## Présentation du package/du code

Le site web de présentation du package est :

  - pour la version validée (master) :
    <https://drealge.gitlab.io/opm2020/index.html>  
  - pour la version en développement (dev) :
    <https://drealge.gitlab.io/opm2020/dev/index.html>  
  - Le code coverage (commun master/dev) (non fiable, nos tests ne sont
    pas optimaux) : <https://drealge.gitlab.io/opm2020/coverage.html>

L’ensemble du code source de l’application, une fois récupéré, devrait
vous permettre de lancer l’application en local. Vous aurez accès à
l’ensemble du code qui sert à notre application.

Par contre, pour y faire des essais, en lançant des bouts de scripts
par-ci par-là, il vous faudra sûrement charger le package (le projet
RStudio mis sous forme de package). Essayez de lancer un “Install and
Restart” dans l’onglet build dans la fenêtre en haut à droite de Rstudio
(habituellement). Si ça ne marche pas, peut-être que lancer un
`devtools::load_all()` fonctionnera (ça permet de charger le projet
RStudio dans l’environnement). Ceci notamment pour que les fonctions que
nous avons créées puissent fonctionner si vous les utilisez en lançant
certains de nos scripts dans les vignettes (pour des essais notamment,
afin de `library(opm2020)` charge bien le package).

Si vous cherchez quelque chose dans l’ensemble des fichiers, n’hésitez
pas à utiliser le Crtl Shift F pour chercher dans l’ensemble des
documents.

<!-- badges: start -->

<!-- badges: end -->

### renv : figer les versions de package utilisées

Nous utilisons le package {renv} pour figer les versions des packages R
que nous utilisons. dans le fichier renv\_instructions.Rmd, vous
trouverez un peu d’explications (à retenir : installer renv avec
`remotes::install_github('rstudio/renv')`, initier renv avec
`renv::init()` puis exécuter `renv::restore()` pour récupérer les
versions des packages que nous utilisons, qui sont répertoriées dans le
fichier **renv.lock**).

### arborescence du projet

L’arborescence des fichiers suit le template golem, un package pour
guider la réalisation d’une application Shiny.
(<https://rtask.thinkr.fr/fr/demarrer-avec-golem/>)

L’arborescence du projet permet plus facilement de s’y retrouver, elle
suit les règles des packages R et des applications shiny développées
avec {golem}.

Notre code est réparti comme ceci :

#### data-raw

Dans **data-raw**, vous trouverez tous les scripts d’imports des données
qui devraient pouvoir être exécutés sans être retouchés (mais ne se
lancent pas avec le bouton knit, parce qu’on utilise des fichiers
temporaires, il faut juste exécuter le script pas à pas), sauf le script
*import\_donnees\_densite\_resid.Rmd* (on se connecte à une base postgre
locale.).  
Ces scripts vont récupérer la donnée disponible sur internet, la
téléchargent, l’importent et la mettent en forme. Nous récupérons les
données concernant le Grand Est, mais avec quelques modifications de
script mineures (code de la région, des départements, nom des fichiers),
vous devriez pouvoir récupérer de la même manière les données de votre
territoire.

Certains scripts d’import de donnnées nécessite d’avoir déjà importer
d’autres données. Il est essentiel d’importer d’abord les données
démographie et banatic (l’import des données banatic s’effectue dans le
script *import\_donnees\_demog.Rmd*) ainsi que les cartes admin\_exress.

#### vignettes

Dans **vignettes**, vous trouverez un fichier Rmd par fonction ou groupe
de fonctions que nous avons réalisées :

  - les indicateurs, débutant par *affiche*,  
  - certains traitements de données,
  - quelques fonctions pratiques (convertisseur de date, retourner les x
    plus grands éléments, supprimer les caractères spéciaux).

Vous retrouverez les html associés à chaque vignette sur le site web de
présentation du package (dans Article. Site web généré automatiquement)
:  
<https://drealge.gitlab.io/opm2020/dev/index.html>  
Ces vignettes expliquent le but de chaque fonction, ce que l’on cherche
à faire, la plupart du temps les choix non techniques que nous faisons,
avec un exemple. Elles ne contiennent pas le code des fonctions.

#### R

On retrouvera le code de chaque fonction dans le script R associé (même
nom que la vignette, mais en .R), dans le dossier **R**.

On documente chacune de nos fonctions avec une documentation {roxygen},
selon un template précis, qui se retrouve dans Référence (sur le site de
présentation du package), en expliquant chaque paramètre et ce que fait
la fonction.

Dans le dossier **R** encore, on retrouve le script
*aa\_chargement\_donnees.R*. Il nous permet actuellement de charger nos
données dans l’application lorsqu’elle est lancée. Nous aurions souhaité
faire autrement (sourcer nos données directement pour qu’elles soient
accessibles sans avoir besoin de les “charger” à chaque fois, mais ce
changement serait chronophage donc on ne le fait pas pour l’instant).
Par la magie de R (qu’on ne maîtrise pas toujours), le simple fait que
ce script soit là permet de charger nos données en lançant l’application
(tout ce qui est dans le dossier R est exécuté en lançant l’appli).

Dans le dosser **R** toujours, vous trouverez des script
*mod\_blabla.R*, ce sont les scripts des différents onglets de
l’application shiny (que l’on ordonne dans *app\_ui.R* et
*app\_server.R*), disposant chacun d’une partie *ui* et d’une partie
*server.* La partie *ui* agence les box contenant les indicateurs et la
partie *server* génère le contenu de chaque box et éventuellement
exécute certains traitements préalables au calcul et à l’affichage des
indicateurs.

#### inst

Dans **inst**, on y retrouve :

  - nos données utilisées dans l’application, en .rds (une fois
    importées en exécutant les scripts de data-raw),  
  - nos autres objets (logo, notice d’utilisation de l’application en
    pdf),  
  - nos fichiers texte qu’on veut faire apparaître dans l’appli (texte
    d’accueil, glossaire, à propos).  
  - notre script Rmd du portrait final qui pourra être téléchargé en
    html dans l’appli (sur le territoire de notre choix),

#### test

Dans **test**, sont sensés apparaître l’ensemble des tests unitaires de
chaque fonction que nous avons développées, afin de s’assurer de leur
fonctionnement au travers du temps. Cependant, peu à l’aise sur ce
sujet, les tests qui y figurent sont souvent sommaires (on vérifie qu’on
produit bien un tableau au lieu de vérifier que le tableau produit est
bien exactement celui attendu). Pour améliorer ceci, il nous faudrait
stocker les objets attendus lors de l’exécution de chaque fonction
testée avec des paramètres définis, puis vérifier que la fonction
retourne bien exactement le même objet que celui qui avait été stocké
comme étant la valeur attendue.

## Quelques spécificités

  - Chaque indicateur est calculé puis stocké dans une reactive\_value
    (pour chaque indicateur, apparaît dans le script de l’onglet
    concerné, mod\_onglet.R). C’est cette reactive\_value qui est
    affichée dans l’application puis dans le portrait en html généré par
    la suite.  
  - Les cartes leaflets proposées dans l’application nécessitent de
    récupérer une image statique (faire un `mapshot`, du package
    *mapview*) de chaque carte pour les incorporer dans le portrait html
    final. Il s’agit donc de récupérer les limites de la carte pour la
    reproduire et la figer.
