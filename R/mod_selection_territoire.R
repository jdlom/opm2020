#' selection_territoire UI Function
#'
#' @description Module Shiny representant l'onglet page d'accueil de l'application.
#' Cette page propose :
#' - un texte d'introduction presentant l'application
#' - deux listes deroulantes de selection de territoire :
#'    - une liste deroulante permettant de selectionner un departement
#'    - une liste deroulante permettant de selectionner un EPCI apres avoir selectionne un departement
#' - une carte du Grand Est a la maille EPCI,  permettant de visualiser les EPCI.
#' - une zone de texte libre pour que l'utilisateur de l'application puisse y ajouter ses propres commentaires.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList selectInput fluidPage fluidRow
#' @importFrom shiny checkboxInput reactiveValues observeEvent
#' @importFrom shiny plotOutput renderPlot htmlOutput
#' @importFrom shinydashboard tabItem box
#' @importFrom shinycssloaders withSpinner
#' @importFrom dplyr %>% arrange filter mutate pull select
#' @importFrom leaflet leaflet renderLeaflet highlightOptions clearShapes clearGroup
#' @importFrom leaflet leafletOutput addTiles addScaleBar addPolygons leafletProxy
#' @importFrom htmltools htmlEscape
#' @importFrom sf st_transform
#'
mod_selection_territoire_ui <- function(id){
  ns <- NS(id)

  tabItem(tabName = "choix1",
          # on initie shinyjs pour pouvoir utiliser les fonctionnalites hidden et show
          # qui permettent de cacher et faire apparaitre des objets
          # shinyjs::useShinyjs(),
          fluidPage(
            # Box texte d'accueil
            box(width = 12,
                htmlOutput(ns("texte_accueil_html")) #si l'on decide de l'integrer dans un fichier texte, par facilite
           ), # box
            fluidRow(
              # Box listes deroulantes selection du territoire
              box(width = 4,
                  # liste deroulante selection du departement
                  selectInput(
                    inputId = ns("select_dep"),
                    label = h4("Choix du d\u00e9partement"),
                    choices = c("votre d\u00e9partement", liste_choix_dep),
                    selected = ""
                  ),
                  # liste deroulante selection de l'EPCI
                  uiOutput(ns("choix_epci"))  # selectInput mis a jour
              ),# fin box

              # Box Carte de la region a la maille EPCI, permettant de visualiser
              # le territoire choisi. Carte leaflet zoomable, nom des EPCI s'affiche lors du survol
              box(width = 8,
                  solidHeader = T,
                  collapsible = T,
                  title = "Carte de localisation",
                  status = "primary",
                  withSpinner(
                    leafletOutput(ns("carteregion"))
                    ),
                  footer = millesime_cog,

                  checkboxInput(inputId = ns("chk_export_carte_region"),
                                label = "Export de la carte (long)",
                                value = F)
              )
            ),

            # box a activer pour tester. Mais ne sera pas conservee
            fluidRow(
              box(width = 12,
                  title = "Saisie de commentaires",
                  box(width = 6,
                      textAreaInput(inputId = ns("texte_libre_accueil"),
                                    label = "",
                                    value = "",
                                    resize = "vertical")
                  )
                  ,
                  box(width = 6,
                      title = "Commentaire mis en forme",
                      htmlOutput(ns("html_texte_libre_accueil"))
                  )
              ) # box comments

            ) # fluidrow
          ) # fluid age
  )
}


#' selection_territoire Server Function
#'
#' @noRd
#' @importFrom markdown renderMarkdown
#' @importFrom mapview mapshot
#' @importFrom leaflet leaflet fitBounds renderLeaflet highlightOptions clearShapes
#' @importFrom leaflet leafletOutput addTiles addScaleBar addPolygons leafletProxy
#' @importFrom sf st_transform st_touches

mod_selection_territoire_server <- function(input, output, session, global){
  ns <- session$ns

  # declaration des variables globales (partagees ) au niveau du module
  local <- reactiveValues()

  # etat des boutons du module export

  global$state_btn_finalise <-  "hideid"
  global$state_btn_daonlode <-  "hideid"


  # nom du fichier de la carte des EPCI a importer dans le Rmd.
  # Si est "", ne s'affiche pas
  global$nf_carte_region <- ""

  # sauvegarde de la carte de la region  dans un objet global a l'appli (reactiveValue au niveau de l'appli)
  # la carte est modifiable par la suite
  global$carte_lf <- leafletProxy("carteregion")


  # Definition du texte d'accueil.
  output$texte_accueil_html <- renderText(
        renderMarkdown(text = texte_accueil)
        )

  # Ce qu'il se passe lorsqu'on choisi un departement via la liste deroulante
    observeEvent(input$select_dep, {

    if(input$select_dep == "") {
      # Si on ne choisit aucun departement, la liste des EPCIs du departement est definie comme vide
      # le code du departement choisi est defini comme vide.
      local$liste_epci_dep <- ""
      global$code_dep_choisi <- ""
    } else {
      #Sinon

      # On etablit la liste des EPCI en fonction du departement choisi dans le selectInput.
      # On modifie le contenu de la variable  `local$liste_epci_dep` avec une liste nommee
      local$liste_epci_dep <- l_epci_r44 %>%
        filter(dep_siege == input$select_dep) %>%
        arrange(lib_epci) %>%
        pull(code_epci)
      names(local$liste_epci_dep) <- l_epci_r44 %>%
        filter(dep_siege == input$select_dep) %>%
        arrange(lib_epci) %>%
        pull(lib_epci)

      # On cree la couche geographique correspondant au contour du departement choisi
      local$carte_dep_choisi <- carte_dep_wgs %>%
        filter(code_dep == input$select_dep)

      # On sauvegarde le code du departement choisi dans une variable globale a toute l'appli
      global$code_dep_choisi <- input$select_dep

      # On retire eventuellement les couches de la carte de la region
      # correspondant au departement et a l'epci precedemment selectionnes
      #  (qui sont identifies sur la carte par des couleurs differentes)
      clearGroup(map = global$carte_lf, group = "dep_choisi")
      clearGroup(map = global$carte_lf, group = "epci_choisi")

      # On rajoute a la carte de la region la couche correspondant au contour du departement choisi :
      # on fait ressotir le departement choisi en vert sur la carte.
      global$carte_lf %>%
        addPolygons(data = local$carte_dep_choisi,
                    group = "dep_choisi",
                    color = "green")

    } # if else
      # masquer les boutons d'apercu et de chargement du portrait si on change de territoire
      # shinyjs::hide("js_preview")
      # shinyjs::hide("js_dwnld_html")
      # message('observeEvent(select_dep): hide("js_dwnld_html")')

  }) # observeEvent(select_dep)

    # Ce qu'il se passe lorsqu'on choisi un EPCI via la liste deroulante
  observeEvent(input$select_epci, {

    # # etat des boutons pour l'export
    # global$state_btn_finalise <-  "showid"
    # global$state_btn_daonlode <-  "hideid"
    # URL des liens vers CoMPTAGE, FLUO et ATMO, utilises dans md_services
    # et dans le modele Rmd
    global$url_trafic_toutier <-
      "http://carto.geo-ide.application.developpement-durable.gouv.fr/1205/Trafic_TMJA_2017-2018_R44.map"

    global$url_atmo <- "http://www.atmo-grandest.eu/"

    global$url_fluo <- "https://www.fluo.eu/"

    if(input$select_epci == "") {
      # Si on ne choisit aucun EPCI :
      # le libelle de l'epci choisi est defini comme vide, la carte des communes de l'EPCI est defini comme vide
      # global$code_epci_choisi <- "" # fait planter l'appli. Cause inconnue
      global$lib_epci_choisi <- ""
      global$carte_communes_choisies <- NULL

      # etat des boutons pour l'export
      global$state_btn_finalise <-  "hideid"
      global$state_btn_daonlode <-  "hideid"
    } else {    # Sinon, si EPCI choisi

      #    # etat des boutons pour l'export
      global$state_btn_finalise <-  "showid"
      global$state_btn_daonlode <-  "hideid"

      # identifie REG et DEP de l'EPCI choisi
      # et on sauvegarde leur code dans des variables globales a l'appli
      temp_reg_dep <- l_epci_fr %>%
        cherche_reg_dep_epci(input$select_epci)
      global$c_reg_epci <- temp_reg_dep %>%
        pull(reg_siege)
      global$c_dep_epci <- temp_reg_dep %>%
        pull(dep_siege)

      # On sauvegarde le code de l'EPCI choisi dans une variable globale a toute l'appli
      global$code_epci_choisi <- input$select_epci

      # On sauvegarde le libelle de l'EPCI choisi dans une variable globale a toute l'appli
      # Il sera affiche dans tous les modules de l'appli
      global$lib_epci_choisi <- l_epci_r44 %>%
        filter(code_epci == input$select_epci) %>%
        pull(lib_epci)
# browser()
      global$nf_portrait_html <- str_c(ote_accents(global$lib_epci_choisi),".html")

      # On cree la couche carto correspondant au contour de l'EPCI choisi,
      # en wgs : sera affiche avec la carte leaflet
      local$carte_epci_choisi <- carte_epci_wgs %>%
        filter(code_epci == input$select_epci)
      # en lambert 93, pour d'autres indicateurs (dont les cartes de flux), dans une variable globale
      global$carte_epci_choisi_lambert <- carte_epci_r44 %>%
        filter(code_epci == input$select_epci)

      # On récupère également les EPCIs voisins de notre EPCI d'étude, ainsi que les départements associés
      global$carte_epci_voisins_lambert <- carte_epci_r44 %>%
        filter(st_touches(global$carte_epci_choisi_lambert,
                          carte_epci_r44,
                          sparse = F) )

      # On sauvegarde la couche carto des communes de l'EPCI dans une variable globale a l'appli,
      # en lambert 93 (comme les donnees initialement incluses dans l'appli)
      global$carte_communes_choisies_lambert <- carte_communes_r44  %>%
        filter(code_epci == input$select_epci)
      #  en wgs, pour la carte des communes de l'EPCI du module "situation".
      global$carte_communes_choisies <- global$carte_communes_choisies_lambert %>%
        st_transform(crs = 4326)


      # On retire eventuellement de la carte de la region la couche correspondant a l'epci precedemment selectionne
      clearGroup(map = global$carte_lf, group = "epci_choisi")
      # On rajoute a la carte de la region la couche correspondant au contour de l'EPCI choisi :
      # on fait ressotir l'EPCI choisi en orange sur la carte.
      global$carte_lf %>% addPolygons(data = local$carte_epci_choisi,
                               group = "epci_choisi",
                               color = "orange")

    } # if

    # masquer les boutons d'apercu et de chargement du portrait si on change de territoire
    # shinyjs::hide("js_preview")
    # shinyjs::hide("js_dwnld_html")
    # golem::invoke_js("showid",ns("btn_finalise"))
    # golem::invoke_js("hideid",ns("daonlode_html"))
    message('select_territoire : observeEvent(select_epci): hide("daonlode_html")')

  }) # observeEvent()


  # Creation de la liste des EPCI du departement selectionne par le selectInput().
  # Liste avec les codes des EPCI, nommes avec leurs libelles :
  #    Cette liste est alimentee par la fonction observeEvent(input$select_dep) ci-dessus
  output$choix_epci <- renderUI({

# Selection de l'EPCI dans la liste des EPCI du departement
    selectInput(
      inputId = ns("select_epci"),
      label = h4("Choix de l'EPCI"),
      choices =  c("Votre territoire" = "",
                   local$liste_epci_dep),
      selected = ""
    )
  }) # renderUI()

  # stockage de la carte
  # carte_reg <- reactiveValues() # ajouts temporaires suite a remarques de Cervan (ThinkR)

  # Affichage de la carte regionale
  output$carteregion <- renderLeaflet({
    # carte_reg$carte <-
      leaflet() %>%
      addTiles() %>%
      addScaleBar()  %>%
      # Couche des departements en bleu
      addPolygons(data = carte_dep_wgs, color = "blue", group = "dep") %>%
      # Couche des EPCI
      addPolygons(data = carte_epci_wgs, color = "#CCCCCC",
                  weight = 1, group = "epci",
                  smoothFactor = 0.5,
                  # permet de faire apparaitre le nom de l'EPCI lorsqu'on le survole
                  label=~htmlEscape(lib_epci),
                  opacity = 0.5, fillOpacity = 0.3,
                  fillColor = "light blue",
                  highlightOptions = highlightOptions(color = "white",
                                                      weight = 1,
                                                      bringToFront = TRUE)
      )
    # carte_reg$carte
  })  # renderLeaflet()

  # Fonctions  utilisees pour le mapshot
  #### Source : https://stackoverflow.com/questions/44259716/how-to-save-a-leaflet-map-in-shiny
  ###
  # Create foundational leaflet map
  # and store it as a reactive expression
  foundational.map.r <- reactive({
    leaflet() %>%
      addTiles() %>%
      addScaleBar() %>%
      addPolygons(data = carte_dep_wgs, color = "blue") %>%
      addPolygons(data = carte_epci_wgs, color = "#CCCCCC",
                  weight = 1,
                  smoothFactor = 0.5,
                  fillColor = "light blue"
      )  %>%
      addPolygons(data = local$carte_epci_choisi,
                  group = "epci_choisi",
                  color = "orange")

  }) # end of foundational.map()

  # store the current user-created version
  # of the Leaflet map for download in a reactive expression
  user.created.map.r <- reactive({
    local$limites <- input$carteregion_bounds
    message(local$limites)
    # call the foundational Leaflet map
    foundational.map.r()  %>%
      fitBounds(lng1 = local$limites$west,
                lng2 = local$limites$east,
                lat1 = local$limites$south,
                lat2 = local$limites$north)

  }) # end of creating user.created.map()


  # export de la carte leaflet
  observeEvent(input$chk_export_carte_region,{
    if(isTRUE(input$chk_export_carte_region)) {

      # nom de fichier d'export qui sera repris dans le modele Rmd
      global$nf_carte_region <- tempfile(pattern = "carte_epci_", fileext = ".jpeg")

      ### Creation du message proposant de patienter pendant l'export
      showModal(modalDialog(
        title = "Export de la carte en cours",
        "Veuillez patienter",
        # easyClose = FALSE,
        footer = NULL,
        fade = TRUE
      ))

      t1 <- Sys.time() # temporaire, pour faire tests de vitesse
      message(str_c("observeEvent(input$chk_export_carte_region",t1))

      # limites de la carte a exporter
      limites <- input$carteregion_bounds
      # basculer en separateur decimal "." pour le mapshot
      options(OutDec = ".")
      mapshot( x = user.created.map.r()
              , cliprect = "viewport"  # utilite pas certaine.

             , file = global$nf_carte_region
      )
      # rebascule apres le mapshot en ","
      options(OutDec = ",")
      message("apres Export carte region")
      message(difftime(Sys.time(), t1)) # temporaire, pour faire tests de vitesse
      removeModal()
    }
  }) # observeEvent(chk)


  # Ce qu'il se passe lorsqu'on ecrit dans la zone de texte libre
  observeEvent(input$texte_libre_accueil, {
    # On sauvegarde/on met a jour immediatement le texte ecrit dans une variable globale a l'appli
    global$texte_libre_accueil <- input$texte_libre_accueil
  })

  # aperçu du texte libre
  output$html_texte_libre_accueil <- renderText(
    renderMarkdown(text = input$texte_libre_accueil)
  )

} # server()


## To be copied in the UI
# mod_selection_territoire_ui("selection_territoire_ui_1")

## To be copied in the server
# callModule(mod_selection_territoire_server, "selection_territoire_ui_1")

