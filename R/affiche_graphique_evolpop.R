
#' Fonction d'affichage de l'evolution de la population en indice 100
#'
#' @param x le tableau des donnees en format tidy long, prepare par la fonction `prepare_indicateur_evolpop()`
#' @param labelsource chaine de caracteres precisant la source des donnees. Affichee en bas du graphique
#' @param ndl chaine de caracteres correspondant a une note de lecture/de bas de page pour le graphique Par defaut, la chaine est vide.
#' @return le graphique au format ggplot
#' @export
#'
#' @importFrom ggplot2 ggplot aes geom_line geom_point element_blank element_text theme theme_bw labs labs ylim
#' @importFrom glue glue
#'
affiche_graphique_evolpop <- function(x, labelsource = "Insee", ndl = "") {
  ggplot(x, aes(x = annee, y = indice_100,
                group = lib_perimetre,
                colour = lib_perimetre)) + theme_bw() +
    geom_line(size=1.0) + geom_point(size = 3) +
    ylim(min(x$indice_100) * 0.95,
         max(x$indice_100) * 1.05) +
    labs(x = "",
         y = "Evolution de la population\nBase 100 en 1968",
         caption = glue("Source : {labelsource}\n{ndl}")) +
    theme(legend.position = c(0.15, 0.85),
          legend.background = element_blank(),
          legend.text = element_text(size = 8),  # cookbook 7
          legend.title = element_blank()
    )
}
