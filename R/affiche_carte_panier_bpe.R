#' Title affiche_carte_panier_bpe
#' Une fonction affichant le nombre d'equipements du panier presents dans chaque commune sous forme d'un point proportionnel au nombre et d'une etiquette indiquant ce nombre.
#'
#' @param x un objet au format sf contenant une colonne `nb_equipements_panier` du nombre d'equipements a l'echelle de la commune
#' @param titre une chaine de caractere correspondant au titre de la carte
#' @param titre_legende une chaine de caractere correspondant au titre de la legende
#' @param labelsource une chaine de caracteres correspondant a la source des donnees, affichee en bas de la carte
#' @param ndl chaine de caractere correspondant a une note de lecture/de bas de page pour le tableau. Par defaut, la chaine est vide.
#'
#' @return une carte au format ggplot
#' @export
#'
#' @importFrom ggplot2 ggplot geom_sf geom_sf_text labs guides theme theme_void
#' @importFrom sf st_centroid
#' @importFrom glue glue

affiche_carte_panier_bpe <- function(x,
                                     titre = "",
                                     titre_legende = "",
                                     labelsource = "",
                                     ndl = "") {
  x %>%
    ggplot() +
    geom_sf(fill = "white")+
    geom_sf(data = st_centroid(x),
            aes(size = nb_equipements_panier),
            color = "red")+
    # theme(legend.title = element_blank()) +
    geom_sf_text(aes(label = nb_equipements_panier)) +
    labs(title = titre,
         size = titre_legende,
         caption = glue("Source : {labelsource}\n{ndl}")
         )  +
    guides(fill = FALSE) +
    # guides(fill = FALSE, size = FALSE) + # masquage de la legende
    theme(legend.position = "none") +
    theme_void()
}


#' affiche_elements_manquants
#' fonction d'affichage des elements manquants parmi les elements du panier
#'
#' @param x un dataframe contenant des colonnes dont le nom commence par `presence_`
#' suivi d'un code devant pouvoir etre retrouve dans la colonne `VAR_ID` du parametre modalites
#' @param modalites le dataframe contenant les modalites des equipements, devant contenir les variables `VAR_ID` et `VAR_LIB`
#'
#' @return la liste des equipements absents ou un message indiquant que tous sont presents
#' @export
#'
#' @importFrom glue glue
#' @importFrom dplyr select summarise mutate filter starts_with left_join add_row
#' @importFrom stringr str_replace
#' @importFrom kableExtra kable kable_styling footnote

affiche_elements_manquants <- function(x,modalites) {
  t_equipements <- x  %>%  as.data.frame() %>%
    select(starts_with("presence")) %>%
    summarise(nom = colnames(.),
              nbe = colSums(.)) %>%
    mutate(nom = str_replace(nom,"presence_","NB_")) %>%
    left_join(modalites, by = c("nom"="VAR_ID")) %>%
    filter(nbe == 0)

  # piedpage <- ifelse(nrow(t_equipements),
  #        "il manque x equipements",
  #        "tous les \u00e9quipements du panier sont presents"
  # )
  if(nrow(t_equipements)==0) {
    t_equipements <- t_equipements %>%
      add_row(VAR_LIB = "tous les \u00e9quipements du panier sont pr\u00e9sents")
  }
  return(t_equipements%>%
           select(`equipements manquants` = VAR_LIB) %>%
    kable(position = "left") %>%
    kable_styling("striped", "bordered",
                  full_width = F) # %>%
    # footnote(general_title = piedpage # ne marche pas
    #          ,
    #          footnote_as_chunk = T)
    )
}


