#' Fonction de preparation des donnees d'evolution de la population avant affichage ou graphique. Selectionne les variables et les fait passer en format tidy long
#'
#' @param x le tableau ayant la structure du tableau agrege, comportant uniquement les lignes a representer
#'
#' @return le tableau en format long des donnees a representer
#'
#' @export
#' @importFrom dplyr arrange ends_with everything filter group_by mutate rename select starts_with ungroup
#' @importFrom stringr str_sub
#' @importFrom tidyr pivot_longer
#' @importFrom readr read_rds
#'
#' @examples
#' library(readr)
#' library(dplyr)
#' d_pop_r44_file <- system.file("donnees_rds/d_pop_r44.rds",
#'                                  package = "opm2020")
#' d_demog_global <- read_rds(d_pop_r44_file)
#' d_demog_agreg <- d_demog_global %>%
#'                     agrege_donnees_demog()
#'
#' code_epci_test <- "240800847"  # CC des Portes du Luxembourg
#'
#'  d_demog_agreg_filter <-
#'                 d_demog_agreg %>%
#'                    filter(code_perimetre == code_epci_test |
#'                               code_perimetre == "08" |
#'                                 code_perimetre == "R44")
#'   d_demog_agreg_filter %>%
#'        prepare_indicateur_evolpop()
#'
prepare_indicateur_evolpop <- function(x) {
  x %>%
    select(ends_with("_perimetre"),
           superficie,
           starts_with("nb_hab")) %>%
    pivot_longer(cols = starts_with("nb_hab"),
                 names_to = "var_cle",
                 values_to = "nb_hab") %>%
    arrange(type_perimetre, var_cle) %>%
    mutate(annee = str_sub(var_cle, -4)) %>%
    group_by(code_perimetre) %>%
    mutate(
           indice_100 = round(nb_hab / nb_hab[1] * 100,1),
           densite_hab = round(nb_hab / superficie, 1)
    ) %>%
    ungroup() %>%
    select(-var_cle) %>%
    select(annee, everything())
}
