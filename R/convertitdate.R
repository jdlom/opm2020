#' convertitdate
#' Convertir les dates d'un format character donné à un format date donné.
#' @param d character ou vecteur de character, coontenant une date au format character
#' @param format_in pattern de la date d'entree.
#' @param format_out pattern du format souhaite en sortie.
#'
#' @return Retourne une date ou un vecteur de dates dans le format souhaite
#' Si le *format_in* passe en parametre ne correspond pas au format d'entree de la date, retourne **NA**.
#' Si le *format_out* ne correspond a aucun format de date, retourne *format_out*.
#'
#' @export
#' @examples
#' ch_date1 <- "2014-01-30"
#' ch_date2 <- "2014-30-01"
#' v_ch_date3 <- c("2009-01-01", "02022010", "02-02-2010")
#' v_ch_date4 <- c("02/27/92", "02/27/92", "01/14/92", "02/28/92")
#' convertitdate(ch_date1, "%Y-%m-%d")
#' convertitdate(ch_date2, "%Y-%d-%m", "%d/%m/%Y")
#' convertitdate(v_ch_date3, c("%Y-%d-%m", "%d%m%Y","%d-%m-%Y"), "%d/%m/%Y")
#' convertitdate(v_ch_date4, "%m/%d/%y", c("%d-%m-%Y", "%d %B %Y", "%d%b%y", "%Y%m%d"))
convertitdate <- function(d, format_in, format_out = "%d %B %Y" ) {
  format(as.Date(d, format = format_in),
         format = format_out)
}
