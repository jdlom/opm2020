#' affiche_tableau_chomage
#' fonction permettant de generer un tableau KableExtra du nombre de chomeurs, du nombre d'actifs
#' et du taux de chomage, pour les perimetres fournis dans les donnees.
#' @param x tibble donnees necessaire pour generer le tableau. Necessaire presence des variables
#' lib_perimetre, chomeurs_15_64, actifs_15_64, taux_chomage
#' @param labelsource chaine de caractere indiquant la source de la donnee. Prevue pour les donnees d'emploi population active, la valeur par defaut est "Insee"
#' @param ndl chaine de caractere correspondant a une note de lecture/de bas de page pour le tableau. Par defaut, la chaine est vide.
#'
#' @return un tableau kableExtra mis en forme, la colonne Perimetre a gauche, alignee a gauche.
#' Les colonnes Chomeurs, Actifs, Taux de chomage (%)  alignees a droite.
#' Une note au pied du tableau concernant la Source et eventuellement du texte.
#' @export
affiche_tableau_chomage <- function(x, labelsource = "Insee", ndl = ""){

  tableau_chomage <- x %>%
    # On ne retient que les variables d'interet : perimetre, nombre de chomeurs, nombre d'actifs, taux de chomage.
    # On arrondi les nombres a l'entier et le taux de chomage a la premiere decimale
    # On renomme les colonnes pour qu'elles soient presentables dans le tableau a afficher
    transmute ("Perimetre" = lib_perimetre,
               "Chomeurs" = format(round(chomeurs_15_64), big.mark   = " "),
               "Actifs" = format(round(actifs_15_64), big.mark   = " "),
               "Taux de chomage (%)" = round(taux_chomage,1)
    ) %>%
    # On cree le tableau, premiere colonne alignee a gauche, les autres alignees a droite.
    kable(align = c("l", rep("r",ncol(.)-1))
    ) %>%
    kable_styling("striped", "bordered",
                  full_width = T) %>%
    footnote(general_title = "Source : ",
             general = glue("{labelsource}\n{ndl}"),
             footnote_as_chunk = T)


  tableau_chomage

}


#' affiche_tableau_conc_emploi
#' fonction permettant de generer un tableau KableExtra du nombre d'actifs occupes dans le territoire,
#' du nombre d'emploi dans le territoire et de l'indice de concentration de l'emploi,
#' pour les perimetres fournis dans les donnees.
#' La premiere colonne du tableau, Perimetre, est alignee a gauche, les autres sont alignees a droite.
#' Une note est presente sous le tableau, indiquant la source, avec possibilite de rajouter une note de lecture.
#' @param x tibble donnees necessaire pour generer le tableau. Necessaire presence des variables
#' lib_perimetre, actifs_occupes, empl_LT, indice_concentration_emploi
#' @param labelsource chaine de caractere indiquant la source de la donnee. Prevue pour les donnees d'emploi population active, la valeur par defaut est "Insee"
#' @param ndl chaine de caractere correspondant a une note de lecture/de bas de page pour le tableau. Par defaut, la chaine est vide.
#'
#' @return un tableau kableExtra mis en forme, la colonne Perimetre a gauche, alignee a gauche.
#' Les colonnes Actifs habitant le territoire, Emploi sur le territoire,
#' Indice de concentration de l'emploi  alignees a droite.
#' Une note au pied du tableau concernant la Source et eventuellement du texte.
#' @export
#'
affiche_tableau_conc_emploi <- function(x, labelsource = "Insee", ndl = "") {

  tableau_conc_emploi <- x %>%
    # On ne retient que les variables d'interet : perimetre, nombre de chomeurs, nombre d'actifs, taux de chomage.
    # On arrondi les nombres a l'entier et le taux de chomage a la premiere decimale
    # On renomme les colonnes pour qu'elles soient presentables dans le tableau a afficher
    transmute ("Perimetre" = lib_perimetre,
               "Actifs occupes habitant le territoire" = format(round(actifs_occupes), big.mark   = " "),
               "Emploi sur le territoire" = format(round(empl_LT), big.mark   = " "),
               "Indice de concentration de l'emploi" = format(round(indice_concentration_emploi), big.mark   = " ")
    ) %>%
    # On cree le tableau, premiere colonne alignee a gauche, les autres alignees a droite.
    kable(align = c("l", rep("r",ncol(.)-1))
    ) %>%
    kable_styling("striped", "bordered",
                  full_width = T) %>%
    footnote(general_title = "Source : ",
             general = glue("{labelsource}\n{ndl}"),
             footnote_as_chunk = T)


  tableau_conc_emploi
}
