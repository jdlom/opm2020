globalVariables(unique(
  c(":=",
  # cherche_reg_dep_epci
  "code_epci", "reg_siege", "dep_siege",
  # agrege_donnees_demog
  "code_dep","code_reg",
  "code_perimetre","type_perimetre", "lib_perimetre",
  # prepare_indicateur_evolpop
  "code_epci_test", "nb_hab","annee", "indice_100", "densite_hab",
  "superficie", "var_cle",
  # affiche_tableau_evolpop
  "lib_perimetre", "annee", "nb_hab", ".",
  # affiche_tableau_densitepop
  "lib_perimetre", "annee", "densite_hab",
  # affiche_pyramide_ages
  "cl_age", "prop_hab", "sexe",
  # calcul_contribution_soldes
  "taux_total_provi", "taux_migration_provi", "taux_naturel_provi",
  "taux_annu_tot_provi", "solde_nat_provi", "solde_tot_provi", "solde_mig_provi",
  "taux", "nature_solde", "plage",
  # affiche_graphique_contribsoldes
  "plage", "taux", "nature_solde",
  # mod_selection_territoire
  "lib_epci", "l_epci_r44", "carte_epci_r44", "l_epcicom_r44",
  # "INSEE_DEP", "CODE_EPCI",
  # affiche_carte_densitepop
  "code_com", "lib_com", "densite", "annee_recente", "superficie", "dens_classes",
  # affiche_banatic
  "date_creation", "date_effet", "civ_president", "prenom_president", "nom_president",
  "adr_siege_1", "adr_siege_2", "adr_siege_3", "code_postal_nom_siege", "tel_siege",
  # affiche_graphique_csp
  "C17_POP15P", "VAR_ID", "VAR_LIB", "nb_pers",
  "lib_categ", "proportion_csp",
  # affiche_tableaux_demog_logement : prepare_indicateur_logement
  "code_perimetre", "type_perimetre", "nb_resid_ppales", "nb_resid_secocc",
  # affiche_tableaux_demog_logement : affiche_tableau_residences
  "lib_perimetre", "nb_logements", "tx_resid_ppales", "tx_resid_secocc",
  # affiche_tableaux_demog_logement : affiche_tableau_logements_vacants
  "lib_perimetre", "nb_logements", "nb_logts_vacants", "tx_logements_vacants",
  # affiche_tableaux_emploi : affiche_tableau_chomage
  "lib_perimetre", "chomeurs_15_64", "actifs_15_64", "taux_chomage",
  # affiche_tableaux_emploi : affiche_tableau_conc_emploi
  "lib_perimetre", "actifs_occupes", "empl_LT", "indice_concentration_emploi",
    # affiche_carte_equipement_auto
  "nombre_de_menages", "menages_1seul_v", "menages_1v_plus", "menages_2v_plus",
  "part_1seul_v", "part_1v_plus", "part_2v_plus",
  "cl_part_1seul_v", "cl_part_1v_plus", "cl_part_2v_plus",
  # affiche_cartes_flux : prepare_donnees_flux
  "code_epci_depart", "code_epci_arrivee", "code_com_depart", "code_com_arrivee",
  "X.epci_depart", "X.epci_arrivee", "Y.epci_depart", "Y.epci_arrivee",
  "X.com_depart", "X.com_arrivee", "Y.com_depart", "Y.com_arrivee", "nb_navettes",
  # affiche_cartes_flux : filter_flux_intra
  "type_flux", "nb_navettes",
  # affiche_cartes_flux : filter_flux_inter
  "type_flux", "nb_navettes",
  # affiche_cartes_flux : affiche_carte_flux_intra
  "code_perim_arrivee", "code_perim_depart", "X.perim_depart", "X.perim_arrivee",
  "Y.perim_depart", "Y.perim_arrivee", "nb_navettes", "cl_nav",
  "code_com", "lib_com", "geometry",
  # affiche_cartes_flux : affiche_carte_flux_inter
  "code_perim_arrivee", "code_perim_depart", "X.perim_depart", "X.perim_arrivee",
  "Y.perim_depart", "Y.perim_arrivee", "nb_navettes", "cl_nav",
  "code_epci", "lib_epci", "geometry",
  # affiche_indicateurs_flux : affiche_tableau_synthese_navettes
  "type_flux", "nb_navettes", "n", "nombre", "proportion",
  # affiche_indicateurs_flux : filtre_flux_epci
  "code_com_depart", "code_com_arrivee", "code_epci_depart", "code_epci_arrivee",
  # affiche_indicateurs_flux : epci_max_echange_flux
  "type_flux", "code_epci_depart", "code_epci_arrivee", "nb_navettes", "code_epci",
  # affiche_indicateurs_flux : affiche_tableau_mode_distance
  "type_flux", "code_epci_depart", "code_epci_arrivee",
  "cl_distance", "mode_transport", "nb_navettes",
  # affiche_indicateurs_flux : affiche_tableau_age_distance
  "type_flux", "code_epci_depart", "code_epci_arrivee",
  "cl_distance", "age_4classes", "nb_navettes",
  # affiche_carte_panier_bpe
  "nb_equipements_panier", "nbe", "nom",
  # prepare_freq_sncf
  "code_epci", "nom_gare",
  # affiche_tableau_gares_sncf
  "total_voyageurs", "moyenne_jour_voyageurs", "nb_jours", "annee", "gare",
  # affiche_tableau_revenus
  "d1_niveau_vie_2017", "d9_niveau_vie_2017", "mediane_niveau_vie_2017",
  # affiche_carte_covoiturage : import_routes_osm
  "highway", "osm_id",
  # affiche_carte_covoiturage : affiche_carte_covoiturage
  "highway", "nom_lieu"


))
)
