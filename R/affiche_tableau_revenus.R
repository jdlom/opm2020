#' fonction d'affichage du tableau des revenus imposables
#'
#' @param x le tableau des donnees contenant l'identifiant des territoires (type_epci), ainsique les colonnes numeriques de revenu median, et des 1er et 9e decile
#'
#' @param labelsource chaine de caractere indiquant la source de la donnee. Prevue pour les donnees de niveau de vie, la valeur par defaut est "Insee - Filosofi"
#' @param ndl chaine de caractere correspondant a une note de lecture/de bas de page pour le tableau. Par defaut, la chaine est vide
#'
#' @return le tableau au format kable (HTML)
#' @export
#'
#' @importFrom dplyr transmute
#' @importFrom kableExtra kable kable_styling footnote
#' @importFrom glue glue
affiche_tableau_revenus <- function(x,
                                    labelsource = "Insee - Filosofi",
                                    ndl = "") {
  x %>%
    transmute(perimetre = type_perimetre,
              mediane = format(mediane_niveau_vie_2017,
                                 big.mark  = " ",
                                 align = "center"),
              decile1 = format(d1_niveau_vie_2017,
                                  big.mark  = " ",
                                  align = "center"),
              decile9 = format(d9_niveau_vie_2017,
                                  big.mark  = " ",
                                  align = "center")
    ) %>%
    kable(col.names = c("P\u00E9rim\u00E8tre", "M\u00E9diane",
                        "1er d\u00E9cile","9e d\u00E9cile"),
      align = c("l", rep("r",ncol(.)-1))
    ) %>%
    kable_styling("striped", "bordered",
                  full_width = T) %>%
    footnote(general_title = "Source : ",
             general = glue("{labelsource}\n{ndl}"),
             footnote_as_chunk = T)
}
