#' emploi UI Function
#'
#' @description Module shiny repr\u0009sentant la page/onglet acces a l'emploi
#' Chaque objet est imbriqu\u0009 dans une box
#' Chaque box comprendra une checkbox (qui permettra d'exporter l'indicateur dans le portrait final).
#' - tableau de concentration de l'emploi
#' - tableau du taux de chomage
#' - Cartes et tableaux des navettes domicile-travail

#' - zone de saisie de texte libre
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#' @import shiny
#' @importFrom shinydashboard tabItem box
#' @importFrom shinycssloaders withSpinner
#' @importFrom htmltools HTML

mod_emploi_ui <- function(id){
  ns <- NS(id)

  tabItem(tabName = "emploi",
          fluidPage(
            fluidRow(
              box(width = 6,
                  solidHeader = T,
                  collapsible = T,
                  title = "Concentration de l'emploi",
                  status = "primary",
                  withSpinner(tableOutput(ns("tableau_conc_emploi")))
                  # ,
                  # checkboxInput(inputId = ns("chk_export_tableau_conc_emploi"),
                  #               label = "Export du tableau",
                  #               value = F)
              ), # box comments
              box(width = 6,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Taux de ch\u00f4mage",
                  withSpinner(tableOutput(ns("tableau_taux_chomage")))
                  # ,
                  # checkboxInput(ns("chk_export_tableau_taux_chomage"),
                  #               "Export du tableau")
              ), # box comments
            ), # fluidrow
            fluidRow(
              box(
                width = 6,
                solidHeader = TRUE,
                collapsible = T,
                status = "primary",
                title = "Flux domicile-travail - Synth\u00e8se",
                withSpinner(tableOutput(ns("tableau_mobpro_synthese")))
                # ,
                # checkboxInput(ns("chk_export_tableau_mobpro_synthese"),
                #               "Export du tableau")
              ) # box
            ), # fluidRow

            fluidRow(
              box(width = 9,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Carte des flux domicile-travail sortant de l'EPCI",
                  withSpinner(plotOutput(ns("carte_mobpro_sortant_epci")))
                  # ,
                  # checkboxInput(ns("chk_export_carte_mobpro_sortant_epci"),
                  #               "Export de la carte")
              )
            ), # fluidrow
            fluidRow(
              box(width = 9,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Carte des flux domicile-travail entrant dans l'EPCI",
                  withSpinner(plotOutput(ns("carte_mobpro_entrant_epci")))
                  # ,
                  # checkboxInput(ns("chk_export_carte_mobpro_entrant_epci"),
                  #               "Export de la carte")
              )
            ), # fluidrow
            fluidRow(
              box(width = 9,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Principaux flux domicile-travail intra EPCI",
                  withSpinner(plotOutput(ns("carte_mobpro_intra_epci")))
                  # ,
                  # checkboxInput(ns("chk_export_carte_mobpro_intra_epci"),
                  #               "Export de la carte")
              )
            ), # fluidRow
            fluidRow(
              box(width = 6,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Flux domicile-travail - navettes intra EPCI par mode de transport et distance",
                  withSpinner(tableOutput(ns("tableau_mobpro_intra_transport")))
                  # ,
                  # checkboxInput(ns("chk_export_tableau_mobpro_intra_transport"),
                  #               "Export du tableau")
              ) # box
              ), # fluidrow
            fluidRow(
              box(width = 8,
                  solidHeader = TRUE,
                  collapsible = T,
                  status = "primary",
                  title = "Flux domicile-travail - navettes avec EPCI majoritaire par mode de transport et distance",
                  # h4("Flux entrants et sortants"),
                  withSpinner(tableOutput(ns("tableau_mobpro_majoritaire_transport")))
                  # ,
                  # checkboxInput(ns("chk_export_tableau_mobpro_majoritaire_transport"),
                  #               "Export du tableau")
              ) #, # box
              # box(width = 6,
              #     solidHeader = TRUE,
              #     collapsible = T,
              #     status = "primary",
              #     title = "Flux domicile-travail - toutes navettes par mode de transport",
              #     withSpinner(plotOutput(ns("plot_mobpro_total_transport")))
              #     # ,
              #     # checkboxInput(ns("chk_export_plot_mobpro_total_transport"),
              #     #               "Export du graphique")
              # ) # box
            ), # fluidrow
            fluidRow(
              box(title = "Saisie de commentaires",
                  width = 12,
                  box(width =6,
                  textAreaInput(inputId = ns("texte_libre_emploi"),
                                label = "",
                                value = "",
                                resize = "vertical")
                  ),
                  box(width = 6,
                      title = "Commentaire mis en forme",
                      htmlOutput(ns("html_texte_libre_emploi")))
              ) # box comments
            ) # fluidrow
          ) # fluidpage
  ) # tabitem


} # ui

#' emploi Server Function
#' @importFrom shinipsum random_image random_table random_ggplot
#' @importFrom ggplot2 coord_flip labs theme_bw
#' @importFrom dplyr filter mutate case_when
#' @importFrom markdown renderMarkdown
#' @importFrom shiny renderPlot renderText observeEvent
#' @noRd
mod_emploi_server <- function(input, output, session, global){
  ns <- session$ns

  local <- reactiveValues()

  # Lorsqu'on change d'EPCI dans le module de selection du territoire, ce code s'execute
  observeEvent(global$code_epci_choisi,{
    # variable locale utilisee dans les indicateurs emploi de la page courante
    local$pop_active_agreg_filter <-
      pop_active_agreg %>%
      filter(code_perimetre == global$code_epci_choisi |
               code_perimetre == global$c_dep_epci |
               code_perimetre == global$c_reg_epci)

    global$tableau_tx_chomage <-
      affiche_tableau_chomage(local$pop_active_agreg_filter,
                              labelsource = source_insee_rp)

    global$tableau_conc_emploi <-
      affiche_tableau_conc_emploi(local$pop_active_agreg_filter,
                                  labelsource = source_insee_rp,
                                  ndl = ndl_conc_emploi)


    ### MOBILITE PRO
    ## On preprare les donnees pour tous les indicateurs

    # donnees mobilite professionnelles filtrees sur l'EPCI choisi
    local$d_mobpro_epci <- filtre_flux_epci(x = d_mobpro,
                                            code_epci_choisi = global$code_epci_choisi)

    # donnee mobilite pro preparees pour representer les flux
    local$d_mobpro_flux <- prepare_donnees_flux(local$d_mobpro_epci)

    # On recupere tous les flux qui partent et qui arrivent dans une commune de notre EPCI d'étude.
    local$d_mobpro_intra <- filter_flux_intra(x = local$d_mobpro_flux,
                                        break_classes = breaks_classes_flux,
                                        label_break_classes = label_break_classes_flux)

    # On recupere tous les flux qui partent et qui arrivent dans une commune de notre EPCI d'étude.
    local$d_mobpro_inter <- filter_flux_inter(x = local$d_mobpro_flux,
                                        break_classes = breaks_classes_flux,
                                        label_break_classes = label_break_classes_flux)


    ## On cree tous les indicateurs

    # on cree le tableau de synthese des flux mobilite professionnelle
    global$tableau_mobpro_synthese <- local$d_mobpro_epci %>%
      affiche_tableau_synthese_navettes(labelsource = source_insee_rp)

    # Tableau mode de transport x distance
    # intra EPCI
    global$tableau_mobpro_intra_transport <- affiche_tableau_mode_distance(x = local$d_mobpro_epci,
                                  code_epci_choisi = global$code_epci_choisi,
                                  labelsource = source_insee_rp,
                                  ndl = ndl_distances_flux)
    # Inter EPCI
    global$tableau_mobpro_majoritaire_transport <- affiche_tableau_mode_distance(x = local$d_mobpro_epci,
                                  code_epci_choisi = global$code_epci_choisi,
                                  intra = FALSE,
                                  labelsource = source_insee_rp,
                                  ndl = ndl_distances_flux)

    # Les cartes de flux
    # Flux intra
    global$carte_mobpro_intra_epci <- affiche_carte_flux_intra(local$d_mobpro_intra,
                                                               cartes = list(global$carte_communes_choisies_lambert,
                                                                             global$carte_epci_voisins_lambert),
                                                               titre = "Carte des d\u00e9placements domicile-travail\nentre les communes de l'EPCI",
                                                               labelsource = source_insee_rp,
                                                               ndl = ndl_nb_flux)
    # Flux entrants
    global$carte_mobpro_entrant_epci <- affiche_carte_flux_inter2(local$d_mobpro_inter,
                                                                  list(global$carte_epci_choisi_lambert,
                                                                       global$carte_epci_voisins_lambert,
                                                                       carte_epci_r44),
                                                                  sens_flux = "Entrant EPCI",
                                                                  titre = "Carte des d\u00e9placements domicile-travail\ninter EPCIs",
                                                                  labelsource = source_insee_rp,
                                                                  ndl = ndl_nb_flux)
    # Flux sortants
    global$carte_mobpro_sortant_epci <- affiche_carte_flux_inter2(local$d_mobpro_inter,
                                                                  list(global$carte_epci_choisi_lambert,
                                                                       global$carte_epci_voisins_lambert,
                                                                       carte_epci_r44),
                                                                  sens_flux = "Sortant EPCI",
                                                                  titre = "Carte des d\u00e9placements domicile-travail\ninter EPCIs",
                                                                  labelsource = source_insee_rp,
                                                                  ndl = ndl_nb_flux)

    ### END MOBILITE PRO




  }) # observeEvent

  output$tableau_conc_emploi <- function() {
    global$tableau_conc_emploi
  }

  output$tableau_taux_chomage <- function(){
    global$tableau_tx_chomage
  }


  output$tableau_mobpro_synthese <- function(){
    global$tableau_mobpro_synthese
  }


  output$carte_mobpro_sortant_epci<- renderPlot({
    global$carte_mobpro_sortant_epci
    })


  output$carte_mobpro_entrant_epci<- renderPlot({
    global$carte_mobpro_entrant_epci
    })


output$carte_mobpro_intra_epci <- renderPlot({
  global$carte_mobpro_intra_epci
  })


output$tableau_mobpro_intra_transport <- function(){
  global$tableau_mobpro_intra_transport
}

output$tableau_mobpro_majoritaire_transport <- function(){
  global$tableau_mobpro_majoritaire_transport
}

# output$plot_mobpro_total_transport <- renderImage({
#   random_image()
# }, deleteFile = FALSE)

observeEvent(input$texte_libre_emploi, {
  global$texte_libre_acc_emploi <- input$texte_libre_emploi
})

# aperçu du texte libre
output$html_texte_libre_emploi <- renderText(
  renderMarkdown(text = input$texte_libre_emploi)
)

  } # server

## To be copied in the UI
# mod_emploi_ui("id_emploi")

## To be copied in the server
# callModule(mod_emploi_server, "id_emploi")

