library(testthat)
library(lubridate)


ch_date1 <- "2014-01-30"
ch_date2 <- "2014-30-01"

test_that("convertion de date fonctionne", {
  expect_equal( convertitdate(ch_date2, "%Y-%d-%m", "%d/%m/%Y"), "30/01/2014")
})


