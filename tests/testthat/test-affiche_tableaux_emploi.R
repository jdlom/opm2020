library(testthat)
library(readr)
library(dplyr)

# [X] Utiliser l’exemple reproductible

d_test_tableaux_emploi <-  data.frame(
  lib_perimetre = c("EPCI", "Departement", "Region"),
  chomeurs_15_64 = c(10,  8, 100000),
  actifs_15_64 = c(500.13456,  1000, 100000),
  empl_LT = c(500,  1200.025, 200000),
  actifs_occupes = c(3000,  30000, 300000),
  indice_concentration_emploi = c(11.05, 1000, 125.4584),
  taux_chomage = c(15.698, 13.78955, 8.1)
)

tableau_chomage <- d_test_tableaux_emploi %>% affiche_tableau_chomage()
tableau_conc_emploi <- d_test_tableaux_emploi %>% affiche_tableau_conc_emploi()


test_that("tableau chomage is kable", {
  expect_true(is.character(tableau_chomage))
  expect_equal(class(tableau_chomage), c("kableExtra","knitr_kable"))
})

test_that("tableau concentration de l'emploi is kable", {
  expect_true(is.character(tableau_conc_emploi))
  expect_equal(class(tableau_conc_emploi), c("kableExtra","knitr_kable"))
})
