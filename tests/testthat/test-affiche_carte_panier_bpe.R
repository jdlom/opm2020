
library(testthat)
library(readr)
library(dplyr)
library(ggplot2)

test_that("affiche_carte_panier_bpe est une carte", {

  # EPCIs de test
  code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)
  # code_epci_test <- "246700488"  # EMS
  # code_epci_test <- "246700777" # val de Villé

  # chargement des donnees
  d_bpe_panier_r44 <- read_rds(system.file("donnees_rds/d_bpe_panier_r44.rds",
                                           package = "opm2020"))
  var_bpe_panier <- read_rds(system.file("donnees_rds/var_bpe_panier.rds",
                                         package = "opm2020"))

  # carte des communes
  c_communes <- read_rds(
    system.file("donnees_rds/carte_communes_r44.rds",
                package = "opm2020"))

  carte_bpe <- c_communes %>%
    filter(code_epci == code_epci_test) %>%
    merge(d_bpe_panier_r44,
          by = "code_com")  %>%
    affiche_carte_panier_bpe(
      titre = "Test - Nombre d'\u00e9quipements du panier par commune",
      # titre_legende = "Nombre",
      labelsource = "Test"
    )
  expect_type(carte_bpe, "list")
  expect_equal(class(carte_bpe), c("gg", "ggplot"))
})
