library(testthat)

test_that("graphique_contribsoldes est un graphique", {
  # tableau de donnees fictives
  t_soldes <- data.frame(
    nature_solde = c("migratoire", "naturel", "total"),
    plage = "1999_2009",
    code_perimetre = 1:3,
    lib_perimetre = c("EPCI", "Departement", "Region"),
    type_perimetre = c("EPCI", "DEP", "REG"),
    nb_habitants_annee_1999 = c(1000, 10000, 100000),
    nb_habitants_annee_2009 = c(1200, 9000, 200000),
    solde_naturel_plage_1999_2009 = c(120, 700, 2000),
    solde_migratoire_plage_1999_2009 = c(-100, 1000, 20000)
  )
  # construction du graphique
  g_contribsoldes <- t_soldes %>%
    calcul_contribution_soldes(nombre_plages = 1) %>%
    affiche_graphique_contribsoldes()

  expect_type(g_contribsoldes, "list")
  expect_equal(class(g_contribsoldes), c("gg", "ggplot"))
})
