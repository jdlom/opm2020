**L'outil Portrait Mobilité**, développé par la **DREAL Grand Est** permet, à partir d’une liste d’indicateurs, la construction à façon d’un portrait de territoire à l’échelle de l’Epci.
Son intérêt ? Vous permettre de contextualiser et d'objectiver le territoire d'étude.
Cet outil porte sur la thématique Mobilité et permet de répondre aux questions suivantes :
- Qui vit sur ce territoire ?
- Quelles sont les caractéristiques des habitants de ce territoire ?
- Quelles sont leurs pratiques de mobilités sur le territoire ?
- De combien de véhicules disposent les ménages ?
- Quels modes de transports utilisent-ils ? Quels sont leurs déplacements ?

Pour vous aider dans sa prise en main, une **notice d'utilisation** est téléchargeable.
