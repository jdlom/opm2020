

## À Propos  

### DREAL Grand Est

Cette application vous est proposée par la Direction Régionale de l'Environnement Aménagement Logement du Grand Est.  
http://www.grand-est.developpement-durable.gouv.fr/  

Retrouvez notre rubrique Connaissance (publications, données, cartes,...)  
http://www.grand-est.developpement-durable.gouv.fr/connaissance-et-developpement-durable-r27.html  

Article présentant l'outil :  
http://www.grand-est.developpement-durable.gouv.fr/outil-portrait-mobilite-a19915.html  

### Développement

Cette application est développée en R Shiny par le Service Connaissance et Développement Durable de la DREAL Grand Est, formé et accompagné par ThinkR (https://thinkr.fr/). Vous pouvez accéder au code source de l'application sur le dépôt GitLab :  
https://gitlab.com/drealge/opm2020  

### Contenu

La DREAL Grand Est n’est en aucun cas responsable de l’utilisation faite des informations mises à disposition dans cette application, et de tout préjudice direct ou indirect pouvant en découler. Notamment, elle ne pourra être tenue responsable du contenu textuel que les utilisateurs sont invités à insérer dans les documents produits par cette application, via les rubriques "Saisie de commentaires" de chaque onglet.  

Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement, merci de bien vouloir le signaler par courriel en décrivant le problème de la manière la plus précise possible (onglet posant problème, action déclenchante, indicateur visé par l'erreur,...).

### Contact

Pour contacter le service en charge de cette application :
scdd.dreal-grand-est@developpement-durable.gouv.fr





