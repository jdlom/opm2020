---
title: "affiche_tableaux_emploi"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_tableaux_emploi}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```



# Objectifs

- Afficher un tableau presentant en colonne le nombre de chomeurs, le nombre d'actifs et le taux de chomage pour, en ligne, l'EPCI, departement et Region, sur le dernier millesime.  

- Afficher un tableau presentant en colonne les actifs habitants le territoire, les emplois sur le territoire (en valeur) et l'indice de concentration d'emploi ( nbr emploi dans la zone/actifs ayant un emploi dans la zone) pour, en ligne, l'EPCI, departement et Region, sur le dernier millesime.  

- Accompagner le tableau indice de concentration d'emploi d'un renvoi à la page glossaire/aide à la lecture, ainsi que d'une note de lecture "Les travailleurs étrangers ne sont pas intégrés au calcul de l'indicateur de concentration de l'emploi présenté ci-dessus, veiller consulter l'aide à la lecture pour plus de précision"  

- Les tableaux presenteront leur premiere colonne (Perimetre) alignee a gauche et les autres colonnes alignees a droite.

# Prealable

- charger les donnees `pop_active_agreg`, issues de la base de donnees emploi population active de l'Insee, deja agregees aux niveaux EPCI, Departement et Region, sur le dernier millesime disponible.  
- retenir uniquement les donnees concernant les territoires d'interet :
  - l'EPCI selectionne
  - le departement de la ville centre de l'EPCI
  - la region de ce departement

# A effectuer

- [x] mettre en forme les tableaux
- [x] afficher les tableaux



# Chargement des donnees/packages

```{r, include = FALSE}
library(opm2020)
library(readr)
library(dplyr)

# Chemins vers les fichiers du package opm2020
l_epci_fr_file <- system.file("donnees_rds/l_epci_fr.rds", package = "opm2020")
pop_active_agreg_file <- system.file("donnees_rds/pop_active_agreg.rds",
                                 package = "opm2020")

l_epci_fr <- read_rds(l_epci_fr_file)
pop_active_agreg <- read_rds(pop_active_agreg_file)

code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)

```


# Traitements prealables
## zones geographiques choisies
Lorsque 1 EPCI choisi, determiner son departement et sa region de rattachement.

```{r}
reg_dep <-
  l_epci_fr %>% cherche_reg_dep_epci(code_epci_test)
c_reg_epci <- reg_dep %>% 
  pull(reg_siege)
c_dep_epci <- reg_dep %>% 
  pull(dep_siege)
```



## restrictions aux zones geographiques choisies
On ne retient que l'EPCI choisi, son département et sa région.

```{r}
pop_active_agreg_filter <- 
  pop_active_agreg %>% 
  filter(code_perimetre == code_epci_test |
           code_perimetre == c_dep_epci |
           code_perimetre == c_reg_epci)
```


# Mise en forme et affichage des tableaux
## Tableau taux de chomage
```{r}
affiche_tableau_chomage(pop_active_agreg_filter)
```


## Tableau indice de concentration de l'emploi
```{r}

affiche_tableau_conc_emploi(pop_active_agreg_filter, 
                                   ndl = "Les travailleurs étrangers ne sont pas intégrés au calcul de l'indicateur de concentration de l'emploi présenté ci-dessus, veiller consulter l'aide à la lecture pour plus de précision")
```
