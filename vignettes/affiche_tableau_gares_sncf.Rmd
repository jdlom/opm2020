---
title: "affiche_tableau_gares_sncf"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_tableau_gares_sncf}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---


```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  eval = TRUE
)
```

# Objectif
Pour les trois derniers millesimes disponibles, afficher la frequentation des gares de l'EPCI selectionne.  
Le millesime le plus recent a droite.  


# Prealables
- recuperer les donnees SNCF de frequentation des gares.

# A effectuer
- [x] retenir uniquement les donnees concernant les gares de l'EPCI
- [x] retenir les millesimes a afficher
- [x] mettre en forme le tableau
- [x] afficher le tableau

# Chargement des donnees/packages

```{r, include = FALSE}
library(opm2020)
library(readr)
library(dplyr)


# Chemins vers les fichiers du package opm2020
frequentation_sncf_file <- system.file("donnees_rds/frequentation_sncf.rds",
                                 package = "opm2020")

frequentation_sncf <- read_rds(frequentation_sncf_file)

code_epci_test <- "200039865"  # CC Metz

```



# Preparation des donnees
On ne retient que les gares de l'EPCI et uniquement les 3 derniers millesimes.  
On transforme les donnees pour n'avoir qu'une seule colonne annee et une seule colonne total_voyageurs.  
On calcule la frequentation moyenne jounaliere, en divisant la frequentation totale par le nombre de jour dans l'annee correspondante (on tient compte des annees bisextiles).
```{r}
d_freq_sncf_epci <- prepare_freq_sncf(x = frequentation_sncf,
                                      code_epci_choisi = code_epci_test,
                                      nb_millesimes = 3)

```


# On affiche le tableau
Finalement, on se laisse la possiblite d'afficher un tableau de frequentation moyenne journaliere ou de frequentation totale annuelle.
```{r}

affiche_tableau_gares_sncf(x = d_freq_sncf_epci,
                          jour = TRUE)
affiche_tableau_gares_sncf(x = d_freq_sncf_epci,
                          jour = FALSE)
 
```

